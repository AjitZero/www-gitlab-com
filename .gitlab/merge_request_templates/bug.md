<!-- Template for fixing a bug -->

## What is current bug behavior

<!-- Describe how to reproduce it or link the related issue -->


## Related issues

<!-- Mention the issue(s) this MR closes or is related to -->

Closes


/label ~group::static site editor ~bug
