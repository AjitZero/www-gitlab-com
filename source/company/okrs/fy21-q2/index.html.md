---
layout: markdown_page
title: "FY21-Q2 OKRs"
---

This [fiscal quarter](/handbook/finance/#fiscal-year) will run from May 1, 2020 to July 31, 2020.

## On this page
{:.no_toc}

- TOC
{:toc}

## OKR Schedule
The by-the-book schedule for the OKR timeline would be 

| OKR schedule | Week of | Task |
| ------ | ------ | ------ |
| -5 | 2020-03-30 | CEO pushes top goals to this page |
| -4 | 2020-04-06 | E-group pushes updates to this page and discusses it in the e-group weekly meeting |
| -3 | 2020-04-13 | E-group 50 minute draft review meeting |
| -2 | 2020-04-20 | Discuss with the board and the teams |
| -1 | 2020-04-27 | CEO reports give a How to achieve presentation |
| 0  | 2020-05-04 | CoS updates OKR page for current quarter to be active |
| +2 | 2020-05-17 | Review previous and next quarter during the next board meeting |

## OKRs

### 1. CEO: IACV
### 2. CEO: Popular next generation product
### 3. CEO: Great team

## How to Achieve Presentations

* Engineering
* Finance
* Legal
* Marketing
* People
* Product
* Product Strategy
* Sales
