---
layout: markdown_page
title: "Category Direction - Synthetic Monitoring"
---

- TOC
{:toc}

## Synthetic Monitoring
Often times it is helpful to run more thorough tests in the production environment. Checking the responsiveness of page loads and ensuring they function is helpful, but some of the most important workflows often involve manipulating data and taking more substantive actions.

Typically this is done with a script which acts as a synthetic user, taking the actions they would normally take and validating the results.

If we are able to include such tests, we have a much better chance of detecting problems, before or as soon as users start finding them. 

#### Proposal
 
Ideally we would be able to leverage much of the scripted tests and other actions that were built as part of a companies existing CI tests. These could be selenium tests for example, or perhaps other non-browser tests like some subset of an end-to-send test suite like gitlab-qa.

This should allow companies to get extra mileage out of their existing investments in CI, or to utilize similar tools to what they are already used to. For example, E-Commerce sites should be testing the whole end to end workflow: browsing for items, adding them to a cart, checking out, paying, etc.

These tests can then serve as the proverbial canary in the coal mine, detecting problems within workflows that simple page load tests would not.

A potential solution could look like:
* Provide a very simple turnkey solution for testing availability and performance, perhaps using sitespeed.
* Enable customers to utilize GitLab CI YML to provide a flexible starting point, and re-use of investments, for full scripted testing
* Offer GitLab.com's shared runners as an easy external testing point
* [Testing SSL expirations and potentially ciphers](https://gitlab.com/gitlab-org/gitlab-ee/issues/5701)
* Build out global infra to offer shared runners in multiple locations, and the ability to select which ones are desired

## What's Next & Why
1. Utilize an existing tool with minimal, if any, config required to do availability and performance testing of a batch of URL's. Something like sitespeed could be used.
1. For now, rely on a connected runner. Allow setting a tag for which runner type this should run on. (e.g. `external` tag)

## Maturity Plan
* [Base Epic](https://gitlab.com/groups/gitlab-org/-/epics/168)

## Competitive Landscape
Not yet, but accepting merge requests to this document.

## Analyst Landscape
Not yet, but accepting merge requests to this document.

## Top Customer Success/Sales Issue(s)
Not yet, but accepting merge requests to this document.

## Top Customer Issue(s)
Not yet, but accepting merge requests to this document.

## Top Internal Customer Issue(s)
Not yet, but accepting merge requests to this document.

## Top Vision Item(s)
Not yet, but accepting merge requests to this document.
