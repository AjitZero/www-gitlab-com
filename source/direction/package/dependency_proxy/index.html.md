---
layout: markdown_page
title: "Category Direction - Dependency Proxy"
---

- TOC
{:toc}

## Dependency Proxy

Many projects depend on a growing number of packages that must be fetched from external sources with each build. This slows down build times and introduces availability issues into the supply chain. ​​For organizations, this presents a critical problem. By providing a mechanism for storing and accessing external packages, we enable faster and more reliable builds.

In addition, the Dependency Proxy will work hand-in-hand with the planned [Dependency Firewall](/direction/package/dependency_firewall/), which will help to prevent any unknown or unverified providers from introducing potential security vulnerabilities.

- [Issue List](https://gitlab.com/groups/gitlab-org/-/issues?label_name%5B%5D=Category%3ADependency+Proxy)
- [Overall Vision](https://about.gitlab.com/direction/package/)
- [UX Research](https://gitlab.com/groups/gitlab-org/-/epics/593)

This page is maintained by the Product Manager for Package, Tim Rizzi ([E-mail](mailto:trizzi@gitlab.com))

## Usecases listed

1. Provide a single method of reaching upstream package management utilities, in the event they are not otherwise reachable. This is commonly due to network restrictions.
1. Let proxy packages act as a cache for increased pipeline build speeds.
1. Track which dependencies are utilized by which projects when pulled through the proxy. (Perhaps when authenticated with a `CI_JOB_TOKEN`.
1. Audit logs in order to find out exactly what happened and with what code.
1. Operate when fully cut off from the internet with local dependencies.

## What's next & why

We have launched the MVC of the [dependency proxy](https://gitlab.com/gitlab-org/gitlab/issues/7934) with [limited availability](https://gitlab.com/gitlab-org/gitlab/issues/7934#availability). [gitlab-#11548](https://gitlab.com/gitlab-org/gitlab/issues/11548) will remove the dependency on Puma, and utilize [workorse](https://gitlab.com/gitlab-org/gitlab-workhorse) for handling all of the download logic.

[gitlab-#11582](https://gitlab.com/gitlab-org/gitlab/issues/11582), will focus on adding authentication to enable use of the dependency proxy with private projects. [gitlab-#11631](https://gitlab.com/gitlab-org/gitlab/issues/11631), will add the ability to delete items from the Dependency Proxy.

## Maturity Plan

This category is currently at the "Minimal" maturity level, and
our next maturity target is Viable (see our [definitions of maturity levels](/direction/maturity/)).
Key deliverables to achieve this are:

- [Make the Dependency Proxy generally available on GitLab.com](https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/78)

## Competitive landscape

* [Artifactory](https://www.jfrog.com/confluence/display/RTF/Docker+Registry#DockerRegistry-RemoteDockerRepositories)
* [Nexus](https://help.sonatype.com/repomanager3/configuration/repository-management#RepositoryManagement-ProxyRepository)

Artifactory is the leader in this category. They offer 'remote repositories' which serve as a caching repository for various package manager integrations. Utilizing the command line, API or a user interface, a user may create policies and control caching and proxying behavior. A Docker image may be requested from a remote repository on demand and if no content is available it will be fetched and cached according to the user's policies. In addition, they offer support for many of major packaging formats in use today. For storage optimization, they offer check-sum based storage, deduplication, copying, moving and deletion of files.

​​However, since they have focused on solving all possible usecases, there is room for simplification and design improvements. We believe this will allow GitLab to provide a more accessible and easier-to-navigate solution. In addition, we provide added value by combining this with our own CI/CD services, improving speed and having everything on-premise.
​​
## Top Customer Success/Sales issue(s)

The top customer success issue is [gitlab-#11582](https://gitlab.com/gitlab-org/gitlab/issues/11582), which will introduce authentication and allow users to leverage the Dependency Proxy with private projects.

## Top user issue(s)

To improve capabilities for our existing users, we want to deliver [gitlab-#9164](https://gitlab.com/gitlab-org/gitlab/issues/9164) (npm) and [gitlab-#9163](https://gitlab.com/gitlab-org/gitlab/issues/9163) (Maven), which will add support for the dependency proxy to the npm and Maven repository.

## Top internal customer issue(s)

The top internal customer issue is [gitlab-#11631](https://gitlab.com/gitlab-org/gitlab/issues/11631) which will add support for deleting items from the dependency proxy.

## Top Vision Item(s)

Our top vision item is [gitlab-#2480](https://gitlab.com/groups/gitlab-org/-/epics/2480), which will introduce search and make items in the Dependency Proxy easier to discover.
