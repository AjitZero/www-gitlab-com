---
layout: markdown_page
title: "Category Direction - Runner"
---

- TOC
{:toc}

## GitLab Runner

GitLab Runner is the multi-platform execution agent that works with [GitLab CI](/direction/verify/continuous_integration)
to execute the jobs in your pipelines.

- [Issue List](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Category%3ARunner)
- [Overall Vision](/direction/verify)
- [UX Research](https://gitlab.com/groups/gitlab-org/-/epics/592)

Interested in joining the conversation for this category? Please join us in our
[public epic](https://gitlab.com/groups/gitlab-org/-/epics/1546) where
we discuss this topic and can answer any questions you may have. Your contributions
are more than welcome.

## What's Next & Why

The following is a selection of some of the significant features that are on the near term roadmap:

- [Windows Shared Runners](https://gitlab.com/gitlab-org/gitlab-runner/issues/4815) on GitLab.com
- [Mac Shared Runners](https://gitlab.com/groups/gitlab-org/-/epics/1830) on GitLab.com

Today, we provide shared Runners on GitLab.com with Linux. The Windows and Mac Shared Runners will provide our Windows and MacOS community users the tooling to build their projects on GitLab.com without needing to set up and maintain build machines.

- [Autoscaling Runners on AWS Fargate](https://gitlab.com/gitlab-org/gitlab-runner/issues/2972) on GitLab.com. 

While customers have successfully implemented our patterns for Autoscaling GitLab Runner on AWS EC2 instances, there is a need to take advantage of solutions such as Fargate that further reduces the infrastructure management burden. One highly sought after solution, is the ability to use AWS Fargate to automatically launch and scale containers with GitLab Runner to execute pipeline jobs. 

## Maturity Plan

Since this category is already at the "Lovable" maturity level (see our
[definitions of maturity levels](/direction/maturity/)),
we do not have an upcoming target for bringing it to the next level. However, it's important to us that
we defend our lovable status and provide the most value to our user community.
To that end, we are starting to work on collecting community feedback to understand users' pain points with installing, managing, and using GitLab Runners. 
If you have any feedback that you would like to share, you can do so in this [epic](https://gitlab.com/gitlab-org/gitlab/issues/39281). 

Maturing the Windows executor and autoscaler is of particular importance as we improve our support for Windows
across the board. There are a few issues we've identified as being critical to that effort:

- [Create VMs in background to speed-up the autoscaling](https://gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler/issues/32)
- [Support services on Windows Docker containers](https://gitlab.com/gitlab-org/gitlab-runner/issues/4186)
- [Distribute Docker windows image for gitlab-runner](https://gitlab.com/gitlab-org/gitlab-runner/issues/3914)
- [Support named pipes for Windows Docker executor](https://gitlab.com/gitlab-org/gitlab-runner/issues/4295)
- [Use Docker Save to publish docker helper images](https://gitlab.com/gitlab-org/gitlab-runner/issues/3979)
- [Add support for Windows device for Docker executor](https://gitlab.com/gitlab-org/gitlab-runner/issues/3923)

## Competitive Landscape

The Runner is currently evaluated as part of the comprehensive competitive analysis in the [Continuous Integration category](/direction/verify/continuous_integration/#competitive-landscape)

## Top Customer Success/Sales Issue(s)

For the CS team, the issue [gitlab-runner#3121](https://gitlab.com/gitlab-org/gitlab-runner/issues/3121) where orphaned
processes can cause issues with the runner in certain cases has been highlighted as generating
support issues.

## Top Customer Issue(s)

The top requested customer issue that we are investigating is [gitlab-runner#2797](https://gitlab.com/gitlab-org/gitlab-runner/issues/2797). 
The principal goal of this feature request is to enable the execution of a GitLab pipeline on a users' local system so that a developer is able to develop, test, and validate their CI pipelines quickly. 

Additionally, although it is not implemented entirely by the Runner team, implementing Vault integration support in GitLab via [gitlab#28321](https://gitlab.com/gitlab-org/gitlab/issues/28321) is important.

Other popular issues include:

- [gitlab-runner#1809](https://gitlab.com/gitlab-org/gitlab-runner/issues/1809): Use variable inside other variable
- [gitlab-runner#1057](https://gitlab.com/gitlab-org/gitlab-runner/issues/1057): Specify root folders for artifacts
- [gitlab-runner#1107](https://gitlab.com/gitlab-org/gitlab-runner/issues/1107): Docker Artifact caching
- [gitlab-runner#2972](https://gitlab.com/gitlab-org/gitlab-runner/issues/2972): Autoscaling GitLab Runner on AWS Fargate
- [gitlab-runner#2229](https://gitlab.com/gitlab-org/gitlab-runner/issues/2229): Adding Services With Kubernetes Executor
- [gitlab-runner#1736](https://gitlab.com/gitlab-org/gitlab-runner/issues/1736): File/directory creation umask when cloning is `0000`

## Top Internal Customer Issue(s)

There are a few top internal customer issues that we're investigating :

- [gitlab-runner#1042](https://gitlab.com/gitlab-org/gitlab-runner/issues/1042): Create network per build to link containers together
- [gitlab#25969](https://gitlab.com/gitlab-org/gitlab/issues/25969): Allow advanced configuration of GitLab runner when installing GitLab managed apps

## Top Vision Item(s)

### Runner Performance/Availability

As discussed on our [CI/CD vision page](https://about.gitlab.com/direction/cicd/#speedy-reliable-pipelines), one of the themes in support of our long term vision and strategy is to provide speedy, reliable pipelines. A vital enabler of that goal is preventing hourly runner queue time spikes: [gitlab-runner#7814.](https://gitlab.com/gitlab-com/gl-infra/infrastructure/issues/7814) We have also set an [FY20-Q4 OKR](https://gitlab.com/gitlab-com/www-gitlab-com/issues/5341) goal to get CI queue times under 1 minute. In addition, we will continue to focus on investigating and providing improvements for other Runner related performance issues to ensure that we are continuously delivering the performance that you need for your build jobs.

Also related to slowness is the important issue [gitlab-runner#4513](https://gitlab.com/gitlab-org/gitlab-runner/issues/4513) which tracks general improvement to execution time.

### Offer Premium Machine Types in Shared Runner Fleet

We currently offer a single size for runners in our shared fleet, but some jobs take more or less CPUs, and some take more or less RAM. Offering additional options will help users who are not comfortable with or not interested in running their own runners.

To follow along, or add feedback to this critical topic, refer to [Epic #2426](https://gitlab.com/groups/gitlab-org/-/epics/2426)

### Shared runner billing and management for self-managed

We are also exploring enabling users of self-managed GitLab instances (CE and EE) to purchase CI minutes, [epic #835.](https://gitlab.com/groups/gitlab-org/-/epics/835) This solution will also include management and reporting capability so that users can see a clear history of Runner minutes granted and consumed.


### Migrating from Docker Machine for Runner Autoscaling

This year, a vital strategic initiative is migrating away from Docker Machine for autoscaling. 
As of now, the approach that we have in mind is to continue to make the minimum viable changes to the Docker Machine autoscaler to support the current user community, while in parallel invest in the Kubernetes executor as the long term replacement solution.

To follow along, or add feedback to this critical topic, check out issue [gitlab-runner#4338](https://gitlab.com/gitlab-org/gitlab-runner/issues/4338)

### Additional Platforms

A common request we get is to add support for various platforms to the Runner. We've chosen to support
the plethora of different systems out there by adding support for a [custom executor](https://docs.gitlab.com/runner/executors/custom.html)that can be overridden to support different needs. In this way platforms like [Lambda](https://gitlab.com/gitlab-org/gitlab-runner/issues/3128), [z/OS](https://gitlab.com/gitlab-org/gitlab-runner/issues/3263), [vSphere](https://gitlab.com/gitlab-org/gitlab-runner/issues/2122), and
even custom in-house implementations can be implemented.

Supporting GitLab runners on IBM z/OS for IBM mainframes, [gitlab-runner#3263](https://gitlab.com/gitlab-org/gitlab-runner/issues/3263) is another issue that is gaining interest from the community. If this is of interest to you, please head over and join the conversation as we would love to get your feedback on this important vision item.

Other platforms of interest we're tracking, through support via the custom executor, include ARM ([gitlab-runner#2076](https://gitlab.com/gitlab-org/gitlab-runner/issues/2076) compatibility, and [GitLab Runners on Google Cloud Run](https://gitlab.com/gitlab-org/gitlab-runner/issues/5028).

These additional platforms are primarily being implemented through community contributions, so if you're interested in contributing to GitLab these issues are great ones to get involved with.
