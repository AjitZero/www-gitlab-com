---
layout: handbook-page-toc
title: "UX Research"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## UX research at GitLab

The goal of UX research at GitLab is to connect with GitLab users all around the world and gather insight into their behaviors, motivations, and goals when using GitLab. We use these insights to inform and strengthen product and design decisions.

UX Researchers aren't the only GitLabbers who conduct user research. Other roles, like Product Managers and Product Designers, frequently conduct research too with guidance from the UX research team.

### Quick links to UX research resources

- [UX Research project](https://gitlab.com/gitlab-org/ux-research) 
- [UXR Insights project](https://gitlab.com/gitlab-org/uxr_insights/)
- [UX research coordination at GitLab](/handbook/engineering/ux/ux-research-coordination/)
- [Training resources](/handbook/engineering/ux/ux-research-training/)
- [Qualtrics tips & tricks](/handbook/engineering/ux/qualtrics/)
- [Shared Google Drive for research videos and artifacts](https://drive.google.com/drive/folders/0AH_zdtW5aioNUk9PVA)

### Research methods

We use a wide variety of research methods that include (but are not limited to):

- [Usability testing](https://www.usability.gov/how-to-and-tools/methods/usability-testing.html)
- [User interviews](https://www.usability.gov/how-to-and-tools/methods/individual-interviews.html)
- [Surveys](https://www.usability.gov/how-to-and-tools/methods/online-surveys.html)
- [Competitor analysis](https://medium.com/user-research/competitive-analysis-b02daf26a96e)
- [Card sorts](https://www.usability.gov/how-to-and-tools/methods/card-sorting.html)
- [Tree tests](https://en.wikipedia.org/wiki/Tree_testing)
- [Beta testing](https://www.intercom.com/blog/how-to-run-a-successful-beta)
- [Buy a feature](https://www.innovationgames.com/buy-a-feature/)
- Design evaluation methods
  - [First Click Tests](https://www.usability.gov/how-to-and-tools/methods/first-click-testing.html)
  - [Preference Tests](https://usabilityhub.com/guides/preference-testing)
  - [Five Second Tests](https://usabilityhub.com/guides/five-second-testing)

### Research tools

Product Managers, Product Designers and UX Researchers have access to the following tools:

[Qualtrics: CoreXM with TextiQ](https://www.qualtrics.com/uk/core-xm/) - Used for surveys, screening surveys and contacting members of GitLab First Look. To request access to Qualtrics, [please open an access request](https://gitlab.com/gitlab-com/access-requests/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=) and assign the issue to your manager for approval. Once approved, please assign the issue to `@sarahj`. For further information about Qualtrics, including how to create and style your survey, please visit [Qualtrics tips & tricks](/handbook/engineering/ux/qualtrics/).

[Mural](https://mural.co/) - For online brainstorming, synthesis and collaboration. Please reach out to a UX Design Manager, Christie Lenneville or Sarah Jones for access.

[Calendly](https://calendly.com/) - For scheduling research sessions with users. A basic plan (free) is usually adaquate for Product Managers and Designers. UX Researchers are entitled to a Premium account. Should you wish to upgrade your Calendly account to Premium, please contact `@sarahj`.

[Zoom Pro Account](https://zoom.us/) - We use Zoom to run usability testing sessions and user interviews. All new team members at GitLab automatically receive a Zoom Pro account.

[UsabilityHub](https://usabilityhub.com/) - Used to build design evaluations, such as first click tests, preference tests and five second tests. UsabilityHub should not be used for surveys. A shared login is available in 1Password.

[OptimalWorkshop](https://www.optimalworkshop.com) - Used for card sorts and tree testing. We do not have an ongoing subscription to OptimalWorkshop. We purchase a monthly license as and when required.  

### How to find existing research

- [UXR_Insights project](https://gitlab.com/gitlab-org/uxr_insights) is the single source of truth (SSOT) for all user insights discovered by GitLab’s UX Researchers, Product Designers, and Product Managers. Instead of reports and slide decks, we use issues to document key findings from research studies. Every issue within the UXR_Insights project contains a single insight on a particular topic. Each insight is supported with evidence, typically in the form of a video clip or statistical data. A directory of completed research is available in the project's [ReadMe](https://gitlab.com/gitlab-org/uxr_insights/blob/master/README.md) file.

- [GitLab Community Forum](https://forum.gitlab.com) - The forum is ran by community members. It's a place for community members to share, ask and discuss everything related to GitLab.

- [Zendesk](https://www.zendesk.com) - Zendesk is a ticketing system used by GitLab's Support team to track problems raised by customers. [Zendesk accounts](https://about.gitlab.com/handbook/support/internal-support/#light-agent-zendesk-accounts-available-for-all-gitlab-staff) are available for all GitLab staff.

- [Chorus.ai](https://www.chorus.ai) - Chorus.ai is used by our Sales team to record conversations that they have with GitLab customers. To request access to Chorus.ai, [please open an access request](https://gitlab.com/gitlab-com/access-requests/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=). 

- [GitLab issue tracker](https://gitlab.com/gitlab-org/gitlab/issues)

- Social media and public forums - Such as [Twitter](https://www.twitter.com), [HackerNews](https://news.ycombinator.com/) and [Reddit](https://www.reddit.com/r/gitlab/).  

### How we decide what to research

UX Researchers collaborate with Product Managers to determine the scope and priority of research studies. Where possible, UX Researchers should try to attend planning meetings for their designated groups. 

UX Researchers should proactively offer ways in which they can assist in the delivery of research. They should also suggest and discuss their own ideas for research studies with Product Managers.


### How UX Research, Product Management, and Product Design work together on research (single-stage-group initiatives)

As part of the validation track of the [Product Development Flow](https://about.gitlab.com/handbook/product-development-flow/#validation-track), Product Managers and Product Designers are encouraged to conduct both problem and solution validation research studies.

#### Problem validation

1. Product Manager fills out an [opportunity canvas](https://docs.google.com/document/d/1pTEMcwH10xWilQEnVc65oC6PdC3VMjn2XoARfNTaHkc/edit#) to their best of their ability. Product Managers are encouraged to reach out to UX Researchers for help.

1. Product Manager or Product Designer opens a `Problem validation` research issue using the available template in the [UX Research project](https://gitlab.com/gitlab-org/ux-research/). Once completed, please assign the issue to the relevant [UX Researcher](https://about.gitlab.com/company/team/?department=ux-research-team).

1. Product Manager, Product Designer and UX Researcher meet to discuss the appropriate research methodology, timescales and user recruitment needs. This meeting is also an opportunity to clarify any unanswered questions in regards to the research study's goals and hypotheses. 

The next steps in the process depend on the research methodology chosen.

##### For user interviews

1. Product Manager drafts the discussion guide in collaboration with the Product Designer. When a first draft of the guide is complete, the UX Researcher reviews and provides feedback.

1. Simultaneously, the Product Designer begins crafting a screening survey in Qualtrics. Note: It's important that the screening survey is completed in a timely manner, so that user recruitment can quickly begin. In most cases, user recruitment should begin before the discussion guide is complete.

1. After the screening survey is created, the Product Designer will open a `Recruiting request` issue using the available template in the [UX Research project](https://gitlab.com/gitlab-org/ux-research/) and assign it to the relevant [Research Coordinator](https://about.gitlab.com/company/team/?department=ux-research-team).

1. The Research Cordinator will perform a sense check to make sure your screener will catch the people you’ve identified as your target participants. If there are multiple rounds of review, the Coordinator will pause activities until uncertainty about your screening criteria has been resolved.

1. The person who is leading the interviews, and who has subsequently supplied their Calendly link to the Research Coordinator, is responsible for forwarding user interview invites to the UX Research calendar (`gitlab.com_kieqv96j35mpt8bdkcbriu2qbg@group.calendar.google.com`) and any other interested parties (Product Designers, Product Managers, UX Researchers, etc).

1. By default, Product Managers are responsible for leading (moderating) problem validation interviews with users. If the study is complex in nature, a UX Researcher may volunteer to moderate. UX Researchers have the discretion to decide which research studies they will moderate and which they will observe synchronously or asynchronously.

1. After the interviews are concluded, the moderator opens an `Incentives request` using the available template in the [UX Research project](https://gitlab.com/gitlab-org/ux-research/) and assigns it to the relevant [Research Coordinator](https://about.gitlab.com/company/team/?department=ux-research-team). The Research Coordinator will reimburse participants for their time (payment occurs on Tuesdays and Thursdays).

1. Product Manager and Product Designer work collaboratively to synthesize the data and identify trends, resulting in findings.

1. UX Researcher reviews findings and provides feedback, if needed.

1. Product Manager or Product Designer creates issues in the [UXR_Insights project](https://gitlab.com/gitlab-org/uxr_insights/) documenting the findings. Please refer to the project's ReadMe and `User interview insight` template for instructions on how to do this.

1. UX Researcher sense checks the documented findings.

1. UX Researcher updates the `Problem validation` research issue with links to findings in the UXR_Insights project and, if applicable, unmarks the `Problem validation` research issue as `confidential` before closing it.

1. Product Manager finalizes the opportunity canvas with the synthesized feedback.

1. Product Manager schedules a review of the opportunity canvas with Scott Williamson, Christie Lenneville, and the Product Director for their section. 

1. Once approved, [the design and solution validation stage](https://about.gitlab.com/handbook/engineering/ux/ux-research/#solution-validation) commences.

##### For surveys

1. Product Manager drafts the survey in collaboration with the Product Designer. When a first draft of the survey is complete, the UX Researcher reviews and provides feedback.

1. The Product Designer enters the survey in Qualtrics.

1. Once the survey has been entered into Qualtrics, the Product Designer opens a `Recruiting request` issue using the available template in the [UX Research project](https://gitlab.com/gitlab-org/ux-research/) and assigns it to the relevant [Research Coordinator](https://about.gitlab.com/company/team/?department=ux-research-team).

1. The Research Coordinator distributes the survey to a sample of participants.

1. The Product Manager, with help from the UX Researcher, will review the responses received so far and amend the survey if necessary. The UX Researcher should advise the Research Coordinator when to continue recruitment. Note: Your sample size for the pilot doesn't need to be excessively large. The aim of a pilot is to discover and address any problems, and to check that the survey data you have received so far is meaningful. The size of your pilot sample depends on a couple of things, such as, the diversity of your target audience, the amount of question routing (skip logic) used and so on. If you are unsure of what your recommended pilot sample size should be, ask a UX Researcher or Research Coordinator.


1. The UX Researcher will keep the Research Coordinator informed of the survey's response rate and must notify them when they plan to close the survey (to ensure recruitment doesn't continue on a survey that has been closed).

1. After the survey is closed, the UX Researcher opens an `Incentives request` using the available template in the [UX Research project](https://gitlab.com/gitlab-org/ux-research/) and assigns it to the relevant [Research Coordinator](https://about.gitlab.com/company/team/?department=ux-research-team). The Research Coordinator will reimburse selected participants for their time (payment occurs on Tuesdays and Thursdays).

1. Product Manager and Product Designer work collaboratively to synthesize the data and identify trends, resulting in findings.

1. UX Researcher reviews findings and provides feedback, if needed.

1. Product Manager or Product Designer creates issues in the [UXR_Insights project](https://gitlab.com/gitlab-org/uxr_insights/) documenting the findings. Please refer to the project's ReadMe and `Survey insight` template for instructions on how to do this.

1. UX Researcher sense checks the documented findings.

1. UX Researcher updates the `Problem validation` research issue with links to findings in the UXR_Insights project and, if applicable, unmarks the `Problem validation` research issue as `confidential` before closing it.

1. Product Manager finalizes the opportunity canvas with the synthesized feedback.

1. Product Manager schedules a review of the opportunity canvas with Scott Williamson, Christie Lenneville, and the Product Director for their section. 

1. Once approved, [the design and solution validation stage](https://about.gitlab.com/handbook/engineering/ux/ux-research/#solution-validation) commences.

#### Solution validation

1. Product Designer opens a `Solution validation` research issue using the available template in the [UX Research project](https://gitlab.com/gitlab-org/ux-research/). Once completed, please assign the issue to the relevant [UX Researcher](https://about.gitlab.com/company/team/?department=ux-research-team).

1. Product Manager, Product Designer and UX Researcher meet to discuss user recruitment needs and to clarify the research study's goals and hypotheses. 

1. Product Designer creates a prototype (low or high-fidelity screenshots, or an interactive UI prototype). Product Designers should participate in design reviews to get feedback from Product Management, Engineering, and peers. This is also the time to involve a technical writer for UI text considerations.

1. Product Designer drafts the usability testing script in collaboration with the Product Manager. When a first draft of the script is complete, the UX Researcher  reviews and provides feedback.

1. Simultaneously, the Product Designer begins crafting a screening survey in Qualtrics. Note: It's important that the screening survey is completed in a timely manner, so that user recruitment can quickly begin. In most cases, user recruitment should begin before the usability testing script is complete.

1. After the screening survey is created, the Product Designer opens a `Recruiting request` issue using the available template in the [UX Research project](https://gitlab.com/gitlab-org/ux-research/) and assigns it to the relevant [Research Coordinator](https://about.gitlab.com/company/team/?department=ux-research-team).

1. The Research Coordinator will perform a sense check to make sure your screener will catch the people you’ve identified as your target participants. If there are multiple rounds of review, the Coordinator will pause activities until uncertainty about your screening criteria has been resolved.

1. The Product Designer is responsible for forwarding usability testing invites to the UX Research calendar (`gitlab.com_kieqv96j35mpt8bdkcbriu2qbg@group.calendar.google.com`) and any other interested parties (Product Designers, Product Managers, UX Researchers, etc).

1. Product Designers are responsible for leading (moderating) usability testing sessions. Product Managers, where possible, should observe all research sessions and take note of insights and pain points. Recommendation: Leave some time between your initial and second usability testing session. Use your first testing session as an opportunity to test your script and make amendments if necessary. All remaining participants (4+ participants) should receive the same testing script, or you will struggle to analyze and draw conclusions from the data you have collected.

1. After the usability testing sessions are concluded, the Product Designer opens a `Incentives request` using the available template in the [UX Research project](https://gitlab.com/gitlab-org/ux-research/) and assigns it to the relevant [Research Coordinator](https://about.gitlab.com/company/team/?department=ux-research-team). The Research Coordinator will reimburse participants for their time (payment occurs on Tuesdays and Thursdays).

1. Product Manager and Product Designer work collaboratively to synthesize the data and identify trends, resulting in findings.

1. UX Researcher reviews findings and provides feedback, if needed.

1. Product Designer creates issues in the [UXR_Insights project](https://gitlab.com/gitlab-org/uxr_insights/) documenting the findings. Please refer to the project's ReadMe and `Usability testing insight` template for instructions on how to do this.

1. UX Researcher sense checks the documented findings.

1. UX Researcher updates the `Solution validation` research issue with links to findings in the UXR_Insights project and, if applicable, unmarks the `Solution validation` research issue as `confidential` before closing it.

1. Product Manager updates the opportunity canvas with the synthesized feedback.

1. The Product Manager must articulate success metrics for each opportunity and ensure a plan for product instrumentation and dashboarding are in place.

1. At this point we have validated the problem and solution, and the issue is ready to enter the [build track](https://about.gitlab.com/handbook/product-development-flow/#build-track).


### How UX Researchers work together on research (multi-stage-group initiatives)

Note: A Lead UX Researcher is the person who devises the research brief and provides an initial outline of the study's goals and hypotheses. A Lead UX Researcher can hold any level of seniority and experience, from Research Coordinator to Staff UX Researcher. The Lead UX Researcher is the [DRI](https://about.gitlab.com/handbook/people-group/directly-responsible-individuals/) for the research study. They should communicate that they are the DRI during stakeholder meetings and within research issues.

1. Lead UX Researcher meets with the relevant PM(s), Designer(s), UX Design Manager(s), fellow UX Researcher(s), and Research Coordinator(s) to discuss the goals of the study and the hypotheses they have. The purpose of this meeting is to get buy-in from all stakeholders. If a stakeholder is unable to attend the meeting, record the session, and allow them the opportunity to provide their feedback asynchronously.

1. Lead UX Researcher fills out an [opportunity canvas](https://docs.google.com/document/d/1pTEMcwH10xWilQEnVc65oC6PdC3VMjn2XoARfNTaHkc/edit).

1. Lead UX Researcher opens a `Problem validation` research issue using the available template in the [UX Research project](https://gitlab.com/gitlab-org/ux-research/). 

1. Lead UX Researcher presents opportunity canvas to their manager. If, during this meeting, significant changes are suggested to the project, you should circle back in with the relevant stakeholders and let them know of any updates.

1. Lead UX Researcher drafts the discussion guide in collaboration with other UX Researchers. 

1. Simultaneously, a member of the UX Research team begins crafting a screening survey in Qualtrics. 

1. After the screening survey is created, a member of the UX Research team opens a `Recruiting request` issue using the available template in the [UX Research project](https://gitlab.com/gitlab-org/ux-research/) and assigns it to the relevant [Research Coordinator](https://about.gitlab.com/company/team/?department=ux-research-team).

1. The person who is leading the interviews, and who has subsequently supplied their Calendly link to the Research Coordinator, is responsible for forwarding user interview invites to the UX Research calendar (`gitlab.com_kieqv96j35mpt8bdkcbriu2qbg@group.calendar.google.com`) and any other interested parties (Product Designers, Product Managers, UX Researchers, etc).

1. UX Researchers are responsible for leading (moderating) interviews with users.

1. After the interviews are concluded, the moderator opens an `Incentives request` using the available template in the [UX Research project](https://gitlab.com/gitlab-org/ux-research/) and assigns it to the relevant [Research Coordinator](https://about.gitlab.com/company/team/?department=ux-research-team). The Research Coordinator will reimburse participants for their time (payment occurs on Tuesdays and Thursdays).

1. UX Researchers work collaboratively to synthesize the data and identify trends, resulting in findings.

1. Lead UX Researcher meets with the relevant PM(s), Designer(s), and UX Design Manager(s) to discuss findings and next steps. All UX Researchers involved in the study are encouraged to attend this meeting.

1. Lead UX Researcher creates issues in the [UXR_Insights project](https://gitlab.com/gitlab-org/uxr_insights/) documenting the findings. Please refer to the project's ReadMe and `User interview insight` template for instructions on how to do this.

1. Lead UX Researcher updates the `Problem validation` research issue with links to findings in the UXR_Insights project and, if applicable, unmarks the `Problem validation` research issue as `confidential` before closing it.

1. Lead UX Researcher finalizes the opportunity canvas with the synthesized feedback.

1. Lead UX Researcher schedules a review of the opportunity canvas with Scott Williamson, Christie Lenneville, their manager, and the relevant Product Director for their section(s). 

1. Once approved, [the design and solution validation stage](https://about.gitlab.com/handbook/engineering/ux/ux-research/#solution-validation) commences.

### How to request research

Any GitLabber can open a research request. If you are **not** a Product Manager, Product Designer, or UX Researcher, please open a `Research request` issue using the available template in the [UX Research project](https://gitlab.com/gitlab-org/ux-research/). Once completed, please assign the issue to the relevant [Product Manager](https://about.gitlab.com/handbook/product/categories/), [Product Designer](https://about.gitlab.com/handbook/product/categories/) and [UX Researcher](https://about.gitlab.com/company/team/?department=ux-research-team). The team will review your issue and notify you when/if they plan to proceed with the work.

### Milestones

Like other departments at GitLab, UX Researchers follow the [Product Development Timeline](/handbook/engineering/workflow/#product-development-timeline) and use milestones to schedule their work. Milestones change monthly, [find out the dates for upcoming milestones](https://gitlab.com/groups/gitlab-org/-/milestones).

### Training resources

If you're new to conducting user research or just want a refresher, please refer to the [UX research training resources](/handbook/engineering/ux/ux-research-training/) to help you get started.

Product Designers and Product Managers should complete the [Research Shadowing](/handbook/engineering/ux/ux-research-training/research-shadowing/) process during their onboarding to gain an understanding of how research is conducted at GitLab.

### UX Research label
Both the [GitLab CE project](https://gitlab.com/gitlab-org/gitlab-ce) and [GitLab EE project](https://gitlab.com/gitlab-org/gitlab-ee) contain a `UX Research` label. The purpose of this label is to help Product Designers and Product Managers keep track of issues which they feel may need UX Research support in the future or which are currently undergoing UX Research. 

UX Researchers are not responsible for maintaining the `UX Research` label. The `UX Research` label **should not** be used to request research from UX Researchers. 
