---
layout: handbook-page-toc
title: "GitLab Onboarding"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

Onboarding is incredibly important at GitLab. We don't expect you to hit the ground running from day one.

We highly recommend taking at least two full weeks for onboarding and only in week three starting with team specific onboarding and training. Please feel free to participate in your team's work in your first two weeks, but don't feel like you have to contribute heavily.
All onboarding steps are in the [onboarding issue template](https://gitlab.com/gitlab-com/people-ops/employment/blob/master/.gitlab/issue_templates/onboarding.md) which is part of the [People Ops Employment project](https://gitlab.com/gitlab-com/people-ops/employment/issues).

Each onboarding issue has a main section that contains tasks relevant to all GitLab team-members and a due date of 30 days.   Below the main section are department and role-specific tasks.  Some roles and departments have tasks that link to a supplemental issue template or an additional onboarding page.  Reach out to your [onboarding buddy](/handbook/general-onboarding/onboarding-buddies/) or other GitLab team members if you need help understanding or completing any of your tasks.

The lists below categorize each onboarding resource by its location.


### People Ops onboarding issue template
* [All GitLab team-members](https://gitlab.com/gitlab-com/people-ops/employment/blob/master/.gitlab/issue_templates/onboarding.md#all-gitlabbers)
* [People Managers](https://gitlab.com/gitlab-com/people-ops/employment/blob/master/.gitlab/issue_templates/onboarding.md#for-people-managers-only)
* [Engineering, such as Developers, Build, Infrastructure, etc.](https://gitlab.com/gitlab-com/people-ops/employment/blob/master/.gitlab/issue_templates/onboarding.md#for-engineering-such-as-developers-build-infrastructure-etc-only)
* [Production and database engineering](https://gitlab.com/gitlab-com/people-ops/employment/blob/master/.gitlab/issue_templates/onboarding.md#for-production-and-database-engineering-only)
* [Database Engineering](https://gitlab.com/gitlab-com/people-ops/employment/blob/master/.gitlab/issue_templates/onboarding.md#for-database-engineering-only)
* [Support](https://gitlab.com/gitlab-com/people-ops/employment/blob/master/.gitlab/issue_templates/onboarding.md#for-support-only)
* [Community advocates](https://gitlab.com/gitlab-com/people-ops/employment/blob/master/.gitlab/issue_templates/onboarding.md#for-community-advocates-only)
* [Product Design](https://gitlab.com/gitlab-com/people-ops/employment/blob/master/.gitlab/issue_templates/onboarding.md#product-designers)
* [Frontend Engineering](https://gitlab.com/gitlab-com/people-ops/employment/blob/master/.gitlab/issue_templates/onboarding.md#for-frontend-engineers-only)
* [Product Management](https://gitlab.com/gitlab-com/people-ops/employment/blob/master/.gitlab/issue_templates/onboarding.md#for-product-management-only)
* [Marketing design](https://gitlab.com/gitlab-com/people-ops/employment/blob/master/.gitlab/issue_templates/onboarding.md#for-marketing-design-only)
* [Security](https://gitlab.com/gitlab-com/people-ops/employment/blob/master/.gitlab/issue_templates/onboarding.md#for-security-only)
* [Finance](https://gitlab.com/gitlab-com/people-ops/employment/blob/master/.gitlab/issue_templates/onboarding.md#for-finance-only)
* [People Ops](https://gitlab.com/gitlab-com/people-ops/employment/blob/master/.gitlab/issue_templates/onboarding.md#for-people-ops-only)
* [Recruiting](https://gitlab.com/gitlab-com/people-ops/employment/blob/master/.gitlab/issue_templates/onboarding.md#for-recruiting-only)
* [Core Team members](https://gitlab.com/gitlab-com/people-ops/employment/blob/master/.gitlab/issue_templates/onboarding.md#for-core-team-members-only)
* [Technical writing](https://gitlab.com/gitlab-com/people-ops/employment/blob/master/.gitlab/issue_templates/onboarding.md#for-technical-writers-only)
* [Marketing non-SDR](https://gitlab.com/gitlab-com/people-ops/employment/blob/master/.gitlab/issue_templates/onboarding.md#for-marketing-non-sdrbdr-only)
* [Sales Development Representatives](https://gitlab.com/gitlab-com/people-ops/employment/blob/master/.gitlab/issue_templates/onboarding.md#for-outbound-sdrs-only)
* [Sales](https://gitlab.com/gitlab-com/people-ops/employment/blob/master/.gitlab/issue_templates/onboarding.md#for-sales-only)
* [Customer success](https://gitlab.com/gitlab-com/people-ops/employment/blob/master/.gitlab/issue_templates/onboarding.md#for-customer-success-only)

### Supplemental onboarding issue templates

* [Interviewing training issue](https://gitlab.com/gitlab-com/people-ops/Training/blob/master/.gitlab/issue_templates/interview_training.md)
* [Monitor group onboarding issue](https://gitlab.com/gitlab-org/monitor/onboarding/blob/master/.gitlab/issue_templates/Monitor_Onboarding.md)
* [Becoming a GitLab manager issue](https://gitlab.com/gitlab-com/people-ops/Training/blob/master/.gitlab/issue_templates/becoming-a-gitlab-manager.md)
* [Production engineering onboarding issue](https://gitlab.com/gitlab-com/gl-infra/infrastructure/blob/master/.gitlab/issue_templates/onboarding_template.md)
* [Security products technical onboarding issue](https://gitlab.com/gitlab-org/security-products/onboarding/blob/master/.gitlab/issue_templates/Technical_Onboarding.md)
* [Support agent onboarding issue](https://gitlab.com/gitlab-com/support/support-training/blob/master/.gitlab/issue_templates/Onboarding%20-%20GitLab.com%20Support%20Agent.md)
* [Support engineer onboarding issue](https://gitlab.com/gitlab-com/support/support-training/blob/master/.gitlab/issue_templates/Onboarding%20-%20GitLab.com%20Support%20Engineer.md)

### Additional onboarding pages

* [Consultant onboarding and offboarding](/handbook/general-onboarding/consultants/)
* [Developer onboarding](/handbook/developer-onboarding/)
* [GitLab onboarding buddies](/handbook/general-onboarding/onboarding-buddies/)
* [Merge Request buddies](/handbook/general-onboarding/mr-buddies/)
* [Parental Leave Reentry buddies](/handbook/general-onboarding/parental-leave-buddy/)
* [Onboarding Processes](/handbook/general-onboarding/onboarding-processes/)
* [Quality team onboarding](/handbook/engineering/quality/onboarding/)
* [Sales team onboarding](/handbook/sales/onboarding/)
* [Support team onboarding](/handbook/support/onboarding/)
* [SRE onboarding](/handbook/engineering/infrastructure/sre-onboarding/)
* [Product Designer onboarding](/handbook/engineering/ux/uxdesigner-onboarding/)
* [UX Researcher onboarding](/handbook/engineering/ux/uxresearcher-onboarding/)
