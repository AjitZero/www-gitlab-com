---
layout: handbook-page-toc
title: "Buyer Personas"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}


### Alex - the Application Development Manager

#### Who - Demographics & Environment

* **Background:**  Alex is a front line leader who is in his first 'management' role.  His career evolved from being an individual contributor to a team lead on projects, to now being in a full time 'management' role.  

* **Demographics:**  (need more data) Typically male, with ~6-10 years of experience. Income range depends on industry and location. They can be found in all most all major metropolitan areas where there are development teams.

* **Identifiers** - Demeanor? Communication preferences?:  Alex is still very technical and only one step removed from being an individual contributor.  He tends to get into the details more than his leaders (directors and VPs).  When evaluating solutions, he is more likely to explore demos and dig into the technical details of products he is considering.

* **Company** - Market segment/industry? Size? On-site, distributed? Full Remote?  Alex can work just about anywhere, in most major metropolitan areas around the world.  There are opportunities for remote work, and often he's managing a blended team of developers who are both his employees and developers from external firms.

* **Team Details:** Size? Development Methodology? DevOps adoption path?  He manages a team of 8 to 12 developers who are using an agile approach to plan and manage their work.  They are in the process of automating development processes for building and testing their application(DevOps).

* **Influencers- where do they learn:** Alex reads SD Times, DevOps.com, Hackernoon, StackOverflow, and others.  He frequently checks out CIO Magazine for leadership and bigger picture challenges. Because his organization is focused on cloud transformation, he has been attending AWS conferences and is actively following key webinars that help him learn about how he can better apply technology to meet his business's demands.

#### What - Motivations

* **Goals/motivations:**  His goals are tied to several dimensions.  First, *Business Satisfaction* is a key goal, as he is on point for managing the IT-Business relationship for their team. He's always working with the business to prioritize the backlog and determine what user stories will be in the upcoming sprint.  He's also responsible for managing his budget and ensuring that his team has the right skills and overall team engagement.   He is regularly asked to reinforce the importance of key SDLC and process tasks ranging form quality, documenting code reviews, security, and accurate time tracking.

* **Challenges:** He never has enough resources (time, people, or infrastructure) to deliver on all the competing requests. The business wants lots of new features and capabilities while at the same time, he faces technical debt lingering from years of previous development. He must balance scope, budget, resources, and expectations on a daily basis. Often, it's hard to have an accurate and real time status of individual work, so he is forced to disrupt team members with questions.

* **What can we do to help them achieve goals and overcome their challenges?**
1. Streamline team collaboration and throughput, helping their team deliver more features the business is asking for
1. Simplify their job of tracking and monitoring progress of their team working on development.  With GitLab, they are able to see the actual status of issues and merge requests.
1. Productivity and performance metrics help them improve their teams performance
1. Compliance and reporting

* **Why do we want to connect** Depending on the size of the company where he works, his role in the tool selection process will vary.  
- **SMB**: Likely responsible for the tool selection decision and has budget authority for his team's tools.  
- **MidSize**: Significant influence over the overall tool selection process, however may not be the final purchasing authority.
- **Large** (enterprise): In a large enterprise, there are multiple development teams and development managers.   In *some* situations, they may have autonomy to select tools for their team, however it is more common for teams to have tool purchases centrally managed, and the **Dev Manager** will play a key role in *recommending* a solution, but will not be the final buyer.


* Real Quotes - Examples of things they commonly say about their goals and challenges.
(Need to gather data)

* **Common Objections** - Why wouldn't they buy us?
1. It's not their 'corporate standard', they have other standard tools, and it's above their pay grade to change.
1. They need to justify the effort to convert from an existing legacy tool to GitLab.  The purchase is only one part of their business case, and they need to justify the labor as much as the OPEX they are spending.
1. They are not responsible for the full tool chain, in fact, they may not 'own' any of the end to end tools
1. They don't have a large tool budget, it's a shared function or managed higher up in the organization.

#### Why - Messaging

* **Discover Questions** - questions to ask
1. "Tell me about the business your team supports" - Learn about the underlying business system and technology they support.
1. "What challenges are you facing with keeping up with business requests?"  - Almost ALL development managers don't have enough resources to keep up and are always trying to prioritize what next.
1. "How are you measuring success, are there strategic goals to accelerate delivery or improve efficiency?" - What kind of challenges are they facing
Note: need to add a few more from Command of the Message

* **Messaging** - How describe our product and value drivers to this persona?
1. GitLab is a key to unlocking the true potential of your team to help you meet the needs of your business partners.  GitLab helps teams become more efficient and effective and stop wasting time waiting.  It is easy for developers to maintain context of work from requirements to code to testing and even deployment.  
1. Improve visibility and insight so you can know the true status of deliverables without disrupting your team.
1. Streamline developer on boarding and bring new team members up to speed faster and more efficiently.

* **Elevator pitch** - What's our 1-2 sentence pitch to this persona?
- Help your team become more effective and efficient. Enable them to collaborate and accelerate application delivery, eliminating wasted time switching context from one tool to another, **and** get data and insight into how the team is performing.


---

### Dakota - the Application Development Director
#### Who - Demographics & Environment

* **Background:**  Dakota is a key IT leader who manages and leads several teams of developers supporting a specific set of business applications.  She has both technical and business skills and as a manager she's focused on delivering business innovation.

* **Demographics:**  (need more data) Typically male, with ~9-15 years of experience. Income range depends on industry and location. They can be found in all most all major metropolitan areas where there are development teams.

* **Identifiers** - Demeanor? Communication preferences?:  Dakota is removed from day to day technical details and focuses her days on planning future work, team skill development, business relationships, and overall team performance. She is concerned about cost and feasibility, managing risk, and leading organizational change. When evaluating solutions, she is more likely to focus on value over specific features.  She will rely on her team to dig into the technical details and demos, etc.

* **Company** - Market segment/industry? Size? On-site, distributed? Full Remote?  Dakota can work just about anywhere, in most major metropolitan areas around the world.   She will typically be in Mid size to large organizations where they have a significant IT and Developer team. There are opportunities for remote work, and often he's managing a blended team of developers who are both his employees and developers from external firms.

* **Team Details:** Size? Development Methodology? DevOps adoption path?  She manages 3 to 5 managers, each who has from 8 to 12 team members. They are in the process of automating development processes for building and testing their application(DevOps).

* **Influencers- where do they learn:** Dakota reads CIO Magazine, Gartner, Forrester and other analyst reports. She attends both industry specific conferences and also strategic technology  conferences such as Forrester, Gartner, and others. She is actively looking for ideas and insight into how to improve the efficiency and effectiveness of her teams.

#### What - Motivations

* **Goals/motivations:**  Her top goal is *Business Satisfaction*, and regardless of the technology, she wants to deliver consistent and predictable business results. She has had a long relationship with her business unit and feels as if she is an extension their team. She is responsible for the IT strategy for her business unit and is looking at ways to improve the business value of her systems.  Through her managers, she is responsible for growing and improving her team and how they improve their delivery effectiveness. She manages her team's budget, ensuring that they have the tools, training, and resources to be successfully develop and deliver the business value.

* **Challenges:** She balances her time between strategic planning with her business partners, and also resolving organizational issues and roadblocks her teams are facing. She develops organizational strategies and plans to secure budget and resources for her team.     Compliance, coding standards, quality, production performance and stability

* **What can we do to help them achieve goals and overcome their challenges?**
1. Streamline team collaboration and throughput, helping their team deliver more features the for the business
1. Simplify their job of tracking and monitoring progress of their team working on development.  With GitLab, they are able to see the actual status of issues and merge requests.
1. Productivity and performance metrics help them improve their teams performance
1. Compliance and reporting

* **Why do we want to connect** Depending on the size of the company where she works, her role in the tool selection process will vary.  
- **SMB**: NA
- **MidSize**: Significant influence over the overall tool selection process, and probably will have final purchasing authority.
- **Large** (enterprise): In a large enterprise, there are multiple development teams and development managers. In *some* situations, they may have autonomy to select tools for their teams, however it is more common for tools to be centrally managed, and the **Dev Director** will play a vital and central role in *endorsing and recommending* a solution, where they are the champion that drives the final selection and recommendation.  

* Real Quotes - Examples of things they commonly say about their goals and challenges.
(Need to gather data)

* **Common Objections** - Why wouldn't they buy us?
1. It's not their 'corporate standard', they have other standard tools, and they are not willing to take the risk to promote a change.
1. They need to justify the effort to convert from an existing legacy tool to GitLab. The purchase is only one part of their business case, and they need to justify the labor as much as the OPEX they are spending.
1. They are not responsible for the full tool chain, in fact, they may not 'own' any of the end to end tools.
1. They don't have a large tool budget, it's a shared function or managed higher up in the organization.

#### Why - Messaging

* **Discover Questions** - questions to ask
1. "Tell me about the business your teams support" - Learn about the underlying business system and technology they support.
1. "What challenges are you facing with keeping up with business requests?" - Almost ALL development managers don't have enough resources to keep up and are always trying to prioritize what next.
1. "How are you measuring success? Are there strategic goals to accelerate delivery or improve efficiency?" - What kind of challenges are they facing
Note: need to add a few more from Command of the Message

* **Messaging** - How describe our product and value drivers to this persona?
1. GitLab is a key to unlocking the true potential of your team to help you meet the needs of your business partners.  GitLab helps teams become more efficient and effective and stop wasting time waiting.  It is easy for developers to maintain context of work from requirements to code to testing and even deployment.  
1. Improve visibility and insight so you can know the true status of deliverables without disrupting your team.
1. Streamline developer on boarding and bring new team members up to speed faster and more efficiently.

* **Elevator pitch** - What's our 1-2 sentence pitch to this persona?
- Help your team become more effective and efficient. Enable them to collaborate and accelerate application delivery, eliminating wasted time switching context from one tool to another, **and** get data and insight into how the team is performing.
