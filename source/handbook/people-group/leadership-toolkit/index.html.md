---
layout: handbook-page-toc
title: "Leadership Toolkit"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

The Leadership Toolkit provides tools and resources to assist people managers (1+ direct report) in effectively leading and developing team members. The goal of the toolkit is to enhance clear communication, and increase team member engagement and retention, while leveraging best practices and shared learnings. The toolkit will continually evolve, and we invite you to contribute!

#### [Annual Compensation - Communication Guidelines](/handbook/people-group/leadership-toolkit/compensation-review)
#### [New Manager Enablement Program](/handbook/people-group/learning-and-development/#new-manager-enablement-program)
#### [Workforce planning and SWOT analysis](/handbook/people-group/workforce-planning-and-swot-analysis)