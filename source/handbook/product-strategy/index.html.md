---
layout: handbook-page-toc
title: "Product Strategy"
---

Product Strategy focuses on long-range impact including:

* [Corporate Development](#corporate-devcelopment) / Acquisitions
* [Interactive prototypes](#interactive-prototypes)
* [Landing moonshots](#landing-moonshots)

## Corporate Development

Corporate Development has its [own page](/handbook/acquisitions).

## Interactive prototypes

Create interactive mockups of the future; telling a story that conveys our vision and strategy.

## Landing moonshots

[Moonshots](/direction/#moonshots) are big hairy audacious goals that may take a long time to deliver. Because they are so large, it's hard to see how to deliver on them. Take these daunting items and cut them into iterable issues and then deliver the first MVC.
