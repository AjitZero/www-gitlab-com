---
layout: handbook-page-toc
title: "Resellers Handbook"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Future State 
- For FY2021 the Partner Portal and Deal registration program will undergo a massive upgrade.  This will include a new partner portal with enhanced functionality focusing on simplification of Order placement, Opportunity management, automated Deal Registration and LMS capabilities.  The goal is to ease the current administrative burden placed on the internal and external sales teams, making them more efficient and freeing up their time to do what they do best. 

## On other pages
{:.no_toc}

- [Reseller Onboarding with Checklist](/handbook/resellers/onboarding/)
- [Main Resellers Page](/resellers)

## How to apply to become a GitLab reseller

- Complete GitLab Reseller application located here: [GitLab Reseller Application](https://about.gitlab.com/resellers/program/#form)
- Discussion with GitLab Channel team 
- Complete GitLab Sales & Technical Training courses located here: [GitLab Reseller Webcasts] (https://about.gitlab.com/webcast/reseller/#ondemand-webcasts)
- Complete GitLab Business planning presentation
- Contract neogiations


## Deal Registration and closing a deal

The steps to closing a deal are:

1. [Deal Registration](#deal-registration)
2. [Sign a quote](#gitlab-quote)
3. [Customer accepts EULA](#gitlab-eula)
4. [Customer receives license key](#licence-key-delivery)
5. [Remitting payment to GitLab](#paying-gitlab)


### Deal Registration
{: #deal-registration}

GitLab requires deal registration. This process is to let us know which deals you are working on so that we can plan accordingly, and also helps prevent channel
conflict.

GitLab will not assign any resources, accept an order, give a quote, issue an evaluation license, nor pay commissions if a deal has not been registered.
**We encourage you to register your deal as soon as you begin working on it**

If we reject your registration because another has already registered the deal, and you still fulfill the order, your contract may still entitle you to a fullfillment fee.

You can register a deal only by completing the [deal registration form](https://docs.google.com/forms/d/1ugj5z4le6OJgR5qp5bPBCoxqlExvs2zVFjs67K2pEO4/viewform?usp=send_form)

When you complete the registration, the registration will be evaluated according to the [deal registration approval workflow](https://docs.google.com/drawings/d/1kcN13pM-NQ4SboxTbF1G_Upfi1_9n9RV4zOO2zuHaek).

- If approved we provide you with deal protection
- If approved a GitLab sales employee will be assigned to assist you in that opportunity.  This assistance may include things like:
    - Provide you with sales support and pre-sales technical assistance
    - Providing an evaluation license for the customer.  See [evaluations](#evaluation-licenses-for-prospects) section below
    - Provide you with any other intelligence that we may have on this account; possibly including:
        - GitLab CE usage (if any) from [GitLab Version Check](/handbook/sales/process/version-check/).
        - Other usage elsewhere at that company and their affiliates
        - Any previous history with that customer

### Receive a GitLab reseller quote
{: #gitlab-quote}

Attached below is a sample quote. When you request a quote, you will receive a document that
looks like this. Yours may differ slightly. This quote reflects your reseller price
exclusive of incentive bonus's and is not meant for the end customer. You will need to
generate your own quote to the end customer.

The quote will come with a [Sertifi](http://corp.sertifi.com/) link. E-signing the quote
with Sertifi will initiate the invoicing process and cause our systems to invoice you.
Do not e-sign the quote until you are ready to be invoiced.  Do not e-sign the quote if
your customer will be paying us directly.

Note that we will not generate a quote, or fulfill an order, without an end user contact complete with email, shipping address, and postal code.

![sample reseller invoice](/images/handbook/sample_reseller_invoice.png){: .shadow}

### License Key Delivery
{: #license-key-delivery}

Once the order is invoiced and the customer accepts the EULA, they will be able to download their GitLab license key.  Note that you will not receive a license key, it goes directly to the customer.

### Remitting payment to GitLab
{: #paying-gitlab}

You can arrange for payment either via invoice, or your customer can pay us directly.

###### By Credit Card

If your customer wishes to pay by credit card, you can simply direct them to `/pricing/`.

##### By purchase order

If your customer will be paying us via a Purchase Order, then you must email us a copy of
the PO so we may invoice the customer. Please have this Purchase Order sent to `POfulfillment@gitlab.com` for fulfillment.

#### Paying via Invoice

To pay via invoice, simply e-sign the [reseller quote](#gitlab-quote) to initiate an
invoice then remit payment in USD to the bank listed on your quote.

###  Execute the GitLab EULA
{: #gitlab-eula}

All orders will require an executed [EULA](/terms/print/gitlab_subscription_terms.pdf) when there's a new subscription or an add-on. There are 3 methods of obtaining a EULA:

1. **License Key Deployment**
   The default for reseller orders is that the end customer will receive a link to download their license key. The customer will have to click an acceptance of terms in order to get their key.
2. **Physical signature**
   Some customers may require a fully countersigned document.  

An order is not complete without a signed agreement.

## Evaluation licenses for prospects
{: #evaluation-licenses-for-prospects}

We will issue you a 30 day evaluation license for your prospects if the deal is properly [registered](#deal-registration).  
We can renew this license if your customer needs more time.  
Upon the second request for renewal (the 3rd license), we will assist you with a managed evaluation, where goals are set for the customer to meet, and one of our Solutions Architects will work with you and the prospect to bring them to completion before the 3rd evaluation expires.  

## Marketing

GitLab will strive to support you in your marketing efforts. Marketing activities should be focused on GitLab EE, and will usually fall into 1 of 3 categories:

### Events

If you are participating in an event or talk and will be representing GitLab please add your event to the GitLab [events page](https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/data/events.yml) and submit MR to @emily.

### Event Funds Requests

We would like to sponsor events related to issues and solutions that GitLab users face every day, such as DevOps, open source and collaboration. Meetups, drink ups, hackathons, trade shows, and seminars are all examples of events that we may help you with. Leads gathered at an event that we help fund or promote are required to be shared with us. These leads will be assigned to you in SalesForce.

We will endeavor to provide funds to support up one event per reseller per quarter. We may also be able to provide speakers, promote your event, provide swag, and/or artwork for an event. Please note that a request does not mean that funds will automatically be allocated.  We need to see value in an event to provide support. **Please submit the request for event sponsorship no later than one month before the date of the event.**

*If request for sponsorship is accepted, you will be asked to provide photos of the event, participate in a blog post writing about your of the event, and provide a summary of the number of opportunities/leads gained.*

Please submit your application for event support [here](https://gitlab.com/gitlab-com/resellers/issues/new) and chose the reseller template.

Please submit your application for event support in a new issue in the [reseller page of gitlab.com](https://gitlab.com/gitlab-com/resellers/issues). When it allows you to select a template, choose the reseller template and fill in responses to the categories. All the relevant GitLab parties will be notified once you submit your issue and will be in touch shortly. You will be able to track the progress of your submission in the issue.

### Swag Requests

Please submit your application for GitLab branded swag [here](https://docs.google.com/forms/d/1x2qP8EyEu2Y_XmIt7txudUYh-PP_Tst6hRuNq3a7Ruc/edit?usp=sharing)
We have been known to co-fund co-branded or locally produced SWAG. Send submit your idea [here](https://gitlab.com/gitlab-com/resellers/issues/new) and chose the reseller template.

### Marketing Collateral - Videos

Reseller will be provided access to certain videos to be used in its marketing effort.  Reseller may modify the content of the videos, but only with respect to the length of the video and/or to localize the video for the local language where marketing efforts are being performed.  Reseller may publish the videos (modified or not) on Reseller's website, however it must be on social media, a page dedicated to promoting GitLab, or otherwise approved by GitLab. The GitLab page must also include "About GitLab" [copy](/handbook/marketing/product-marketing/#single-sentence) (the reseller may use judgment as to which length description is appropriate), and  a link to about.gitlab.com.


## Technical Support

While we do not require them to, we do expect that your customers will, for the most part, contact you if they need help.
It is in both of our best interest that they do so, as the more touch points you have with them, the more likely you are to further develop business with them.
We do not expect you to be as knowledgeable about our products as our own support staff, and do expect that you will need to escalate some issues to our support staff.

For pre-sales technical issues, please contact your local GitLab sales team.

## GitLab Landing Page guidelines
{: #landing-page}

Your website will need to have a landing page with information about GitLab. You can see what others have done from the [Resellers page](/resellers).

- We would prefer your landing page to be at `yoururl.com/GitLab` where this is not possible, we would ask you to set a redirect for that URL to the actual one.
- We highly encourage your landing page to be in local language.  There are plenty of English language resources on GitLab, so providing them in the native tongue of your customers adds value.
- You should use our [Authorized Reseller Logo](#Logo) on your page, and have it link back to us at `/resellers`
- There needs to be a _“What is GitLab?”_ paragraph
- Where ever you mention a GitLab product or feature, there should be a link back to our corresponding item on ``

Here is also a list of resources that you may find useful to include on your landing page.

- [What is GitLab]()
- [What is GitLab EE](/features/#enterprise)
- [CE vs EE comparison](/products/#compare-options)
- [GitLab CI](/product/continuous-integration/)
- [GitLab Geo](/solutions/geo/)
- [Pricing](/product/)
- [GitLab Blog](/blog/)
- [GitLab Culture](/company/culture/)
- [Gitlab on Twitter](https://twitter.com/gitlab)
- [GitLab Team Handbook](/handbook/)

## Authorized Reseller Logos
{: #Logo}

The GitLab Authorized reseller logo should help tell your prospects and customers that we are working with you.

You should use our Authorized Reseller Logo on your materials where appropriate and in accordance with our brand guidelines.

The logos are available in the README file of the [resellers project](https://gitlab.com/gitlab-com/resellers/).

## Slack
{: #GitLab Slack Channel}

All authorised gitlab resellers will be invited to the GitLab `#resellers` slack channel.  This is a channel for you to reach our sales and marketing team, as well other resellers.

<style>
  blockquote p {
   font-style: italic !important;
  }
</style>
