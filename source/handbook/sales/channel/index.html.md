---
layout: handbook-page-toc
title: "Channels"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Channels Handbook

The Channel is a critical part of our strategy moving forward as it will help us 1) drive growth ARR through services capacity and capability to drive customer adoption and usage of the Gitlab platform and 2) drive and increase new customer ARR through their relationships, service engagements, and knowledge of accounts.

### Channels KR

[OKRs](/company/okrs/)

### Channels Value 

The Channel is a critical part of our strategy moving forward as it will help us 1) drive growth ARR through services capacity and capability to drive customer adoption and usage of the Gitlab platform and 2) drive and increase new customer ARR through their relationships, service engagements, and knowledge of accounts.

![GitLab Channel Value](/handbook/sales/channel/images/channel_handbook1.png)

## Channels Partner Types 

### Alliances

[Alliances](/handbook/alliances/)

### Resellers 

Primary monetization is through reselling GitLab licenses and services. Resellers can be a service partner too (often known as a Solution Provider).

1. VAR/VAD (Value Added Reseller or Distributor) - Channel services including resale, implementation, contracting, support, financing etc.
2. DMR (Direct Market Reseller)- primary business is resale of the software, often does not implement. Value are the contracts that these partners have in place with customers.

[Learn how to become a Gitlab reseller today](/handbook/resellers/)

### Services partners. 

Primary monetization is through the sale of services. This can be a one-time implementation, ongoing support or advisory, managed services or outsourcing. Services partners will resell Gitlab services, deliver services on behalf of Gitlab or deliver Gitlab certified services.  Services partners can be a reseller partner too (often known as a Solution Provider).

1. Global Systems Integrators - have a large global workforce and can deliver on almost any customer need. Examples: Accenture, Deloitte, TCS, Wipro
2. Regional Systems Integrators - large workforce but with single continental focus and a more limited offering of services. Examples: CI&T, Slalom
3. Boutique Systems Integrators - very focused DevOps partners that could be deep experts on GitLab and the nuances of getting it setup and running it. Examples - CloudReach, Flux7
4. Managed Service Providers - provide ongoing support for solutions/applications. Examples: Rackspace

### Existing Gitlab Partner Program

[Resellers program](/resellers/program/)

### Gitlab Channels Program Updates - March 2020

1. Building a channel of enabled, DevOps & Digital Transformation focused resellers and services providers. 
2. Provide eStore access for SMB & Midmarket channel partners 
3. Net neutral to Gitlab seller compensation 
4. Incentives to identify net new customers & opportunities in existing customers.
5. Incentives to attach product, operational & strategic services.
6. Referral fees for non reselling services partners.
7. MDF available for Demand Generation activities and events
8. Sales & SE Enablement available on demand;  certifications - H2’FY21 
9. Services Certifications - H2’FY21
10. Renewals incumbent protection: If a partner sells a deal and is in good standing (actively supporting the customer, etc) that partner receives first right of refusal for renewal; unless otherwise stated by the customer.

[Internal - Channels Strategy & Program Updates FY21](https://gitlab.com/gitlab-com/sales-team/field-operations/channel/-/wikis/Channel-home)

## Channel Sales Enablement - Core Curriculum 

### Welcome to GitLab! 
*  **What is GitLab? (Material - Video, 3 Minutes)**
   - GitLab is a single application for the entire DevOps lifecycle. [Watch the video](https://www.youtube.com/watch?v=MqL6BMOySIQ).
*  **Everyone Can Contribute (Material - Video, 3 Minutes)**
   - Learn more about how we live out our Contribute value! [Watch the video](https://www.youtube.com/watch?v=V2Z1h_2gLNU).
*  **Org Chart (Material - Handbook - 10 Minutes)**
   - Check out the [org chart](https://about.gitlab.com/company/team/org-chart/) and the [Team Page](https://about.gitlab.com/company/team/)

### DevOps Technology Landscape
*  **The Software Development Lifecycle (Material - Video - 9 Minutes)**
   - This video provides a nice overview of the SDLC. For added context, check out the GitLab page that covers each stage in the life cycle. 
   - [YouTube - Software Development Lifecycle in 9 Minutes! ](https://www.youtube.com/watch?v=i-QyW8D3ei0)
   - [Dev Ops Lifecycle - Handbook](https://about.gitlab.com/stages-devops-lifecycle/)
   - [YouTube - How is Software Made? ](https://www.youtube.com/watch?v=UuX-GnYWNwo)
*  **GitLab SDLC Quiz (Quiz - 9 Points)**
   - Take a look at the [handbook page on the SDLC](https://about.gitlab.com/stages-devops-lifecycle/) and take this short [quiz](https://forms.gle/JPfqXxY7swi5txmf6)

### Our Customers
*  **Personas & Pain Points (Material - Video / Handbook - 20 Minutes)**
   - Introducing Personas and Pain Points. David Dulany, Sales Development Consultant, reviews the concepts of Personas and Pain Points and why they are important in setting the context for your messaging.
   - [You Tube - Introducing Personas and Pain Points](https://www.youtube.com/watch?v=-UITZi0mXeU)
   - [Handbook - Roles & Personas](https://about.gitlab.com/handbook/marketing/product-marketing/roles-personas/)
*  **VP of App Dev (Material - Video - 10 Minutes)**
   - Watch Product Marketing Manager William Chia talk about the VP of App Dev persona.
   - [YouTube - VP App Dev](https://www.youtube.com/watch?v=58qDalA5o6Q)
*  **DevOps Director (Material - Video - 10 Minutes)**
   - [YouTube - Director of DevOps](https://www.youtube.com/watch?v=5_D4brnjwTg)
*  **Head of IT (Material - Video - 10 Minutes)**
   - [YouTube - Head of IT](https://www.youtube.com/watch?v=LUh5eevH3F4)
*  **Chief Architect (Material - Video - 10 Minutes)**
   - [YouTube - Chief Architect](https://www.youtube.com/watch?v=qyELotxsQzY)
*  **GitLab Digital Transformation CxO Discovery Guide (Material - 10 Minutes)**
   - [GitLab Discovery Guide](https://drive.google.com/open?id=1R6is7t4Ph3-p4tGJbDq0RhezT4j-3P0rluMV3H9-Rho&authuser=0)
*  **Customer Success Stories & Proof Points (Material - Handbook - 10 Minutes)**
   - References are an age old tenet of sales pros! Your prospective clients will definitely be impressed by the positive business outcomes of our customers. Check out our [customer case studies](https://about.gitlab.com/customers/) on GitLab value.

### Our Portfolio
* **GitLab Value Framework (Material - Sales Collateral - 35 Minutes)**
   - The GitLab [value framework](https://drive.google.com/open?id=1GV1WGyJIRuor0jxG-9ABu9ZSIBUFtPq1pqAxV9yJOvQ&authuser=0) is one of the most useful tools available for salespeople. Take a look to understand our value drivers, how to uncover customer needs, and how to articulate value and differentiation. A [framework summary](https://drive.google.com/open?id=1BawkSEbejPKx2EVgOqxtrhzHlCTGEFNzvoIF4hW3bT4&authuser=0) is also avaliable for quick reference.
*  **Feature Comparison (Material - Handbook - 10 Minutes)**
   - Check out the [feature comparison](https://about.gitlab.com/pricing/self-managed/feature-comparison/) chart to learn what's included in each package.
*  **Why Sell Ultimate/Gold? (Material - Handbook - 10 Minutes)**
   - Take a look at the [handbook link](https://about.gitlab.com/pricing/ultimate/) to understand what the best plans have to offer!
*  **Pricing (Material - Handbook - 10 Minutes)**
   - Check out the chart to understand our [pricing model](https://about.gitlab.com/pricing/). For additional context take a look at the [handbook page on pricing](https://about.gitlab.com/handbook/ceo/pricing/).
*  **GitLab Direction (Material - Handbook - 10 Minutes)**
   - Our vision is to replace disparate DevOps toolchains with a single application that is pre-configured to work by default across the entire DevOps lifecycle. We aim to make it faster and easier for groups of contributors to deliver value to their users, and we achieve this by enabling: Faster cycle time, driving an improved time to innovation, Easier workflows driving increased collaboration and productivity. Our solution plays well with others, works for teams of any size and composition and for any kind of project, and provides ongoing actionable feedback for continuous improvement. You can read more about the principles that guide our prioritization process in our [product direction handbook.](https://about.gitlab.com/direction/#single-application)
*  **Product Maturity (Material - Handbook - 10 Minutes)**
   - GitLab has a broad scope and vision, and we are constantly iterating on existing and new features. Some stages and features are more mature than others. To convey the state of our feature set and be transparent, we have developed a [maturity framework](https://about.gitlab.com/direction/maturity/) for categories, application types, and stages.
*  **Use Cases (Material - Handbook - 10 Minutes)**
   - A [customer use case](https://about.gitlab.com/handbook/use-cases/) is: A customer problem or initiative that needs a solution and attracts budget. Defined In customer terms: Often aligned to industry analyst market coverage (i.e. Gartner, Forrester, etc. write reports on the topic) These are discrete problems that we believe GitLab solves and are reasons customers choose GitLab (hence which we should seek out in prospects)

### Competitive Advantages & Strategy
*  **Competitor Overview (Material - Handbook - 30 Minutes)**
   - There are a lot of [DevOps tools](https://about.gitlab.com/devops-tools/) out there. As a single application for the entire DevOps life cycle, GitLab can remove the pain of having to choose, integrate, learn, and maintain the multitude of tools necessary for a successful DevOps tool chain. However, GitLab does not claim to contain all the functionality of all the tools listed here. Click on a DevOps tool to compare it to GitLab.

### Channel Sales Support
*  **Reseller Handbook (Material - Handbook - 10 Minutes)**
   - This is definitely bookmark worthy material! The [reseller handbook](https://about.gitlab.com/handbook/resellers/) is your source of truth for everything sales!
* **Gitlab Reseller Webcasts**
   - Take a look at our [reseler webcasts](https://about.gitlab.com/webcast/reseller/) and sign up for a live or on demand session.
*  **GitLab Terms & Conditions (Material - Handbook - 10 Minutes)**
   - The following [terms and conditions](https://about.gitlab.com/terms/) govern all use of the GitLab.com website, or any other website owned and operated by GitLab which incorporate these terms and conditions) (the “Website”), including all content, services and support packages provided on via the Website. The Website is offered subject to your acceptance without modification of all of the terms and conditions contained herein and all other operating rules, policies (including, without limitation, procedures that may be published from time to time on this Website by GitLab (collectively, the “Agreement”).

## Channel Sales Enablement - Advanced Curriculum

### Technical Deep Dive 

* **Auto DevOps**
   - Auto DevOps provides pre-defined CI/CD configuration which allows you to automatically detect, build, test, deploy, and monitor your applications. Leveraging CI/CD best practices and tools, Auto DevOps aims to simplify the setup and execution of a mature & modern software development lifecycle. [Review this document](https://docs.gitlab.com/ee/topics/autodevops/) to learn about each component of GitLab Auto DevOps.
* **GitLab API**
   - [Review this document](https://docs.gitlab.com/ee/api/README.html) to learn more about GitLab API, its capabilities and shortcomings.
* **GitLab for Agile**
   - Agile development is iterative, incremental, and lean approaches to streamline and accelerate the delivery of projects. Ever wondered if GitLab supports Agile methodology? If you're considering using GitLab it might not be obvious how its features correspond with Agile artifacts, so we've broken it down for you in a [blog](https://about.gitlab.com/blog/2018/03/05/gitlab-for-agile-software-development/) and the [GitLab Agile Planning page](https://about.gitlab.com/solutions/agile-delivery/).
* **GitLab Runners**
   - [GitLab Runner](https://docs.gitlab.com/runner/) is the open source project that is used to run your jobs and send the results back to GitLab. It is used in conjunction with GitLab CI, the open-source continuous integration service included with GitLab that coordinates the jobs. Below are slides, the video presentation, and additional information about GitLab Runners.
* **GitLab High Availability (HA) and GitLab GEO**
   - [Review the GitLab HA and Geo Replication Overview](https://about.gitlab.com/solutions/high-availability/), then hear Brian Wald, Solution Architect Manager, break it all down [on YouTube](https://youtu.be/fji7nvmOHNQ).

### Integrations 
* **Category Overview**
   - GitLab's vision is to be the best single application for every part of the DevOps toolchain. However, some customers use tools other than our built-in features–and we respect those decisions. The Integrations category was created specifically to better serve those customers. Currently, GitLab offers [30+ project services](https://docs.gitlab.com/ee/user/project/integrations/project_services.html#project-services) that integrate with a variety of external systems. Integrations are a high priority for GitLab, and the Integrations category was established to develop and maintain these integrations with key 3rd party systems and services.
* **Jira** 
   - GitLab Issues are a powerful tool for discussing ideas and planning and tracking work. However, many organizations have been using Jira for these purposes and have extensive data and business processes built into it. While you can always migrate content and process from Jira to GitLab Issues, you can also opt to continue using Jira and use it together with GitLab through our [integration.](https://docs.gitlab.com/ee/user/project/integrations/jira.html) For a video demonstration of integration with Jira, watch [GitLab workflow with Jira issues and Jenkins pipelines.](https://youtu.be/Jn-_fyra7xQ)
* **Jenkins**
   - GitLab’s [Jenkins integration](https://docs.gitlab.com/ee/integration/jenkins.html) allows you to trigger a Jenkins build when you push code to a repository, or when a merge request is created. Additionally, it shows the pipeline status on merge request widgets and on the project’s home page. Videos are also available on [GitLab workflow with Jira issues and Jenkins pipelines](https://youtu.be/Jn-_fyra7xQ) and [Migrating from Jenkins to GitLab.](https://youtu.be/RlEVGOpYF5Y)
* **Github**
   - GitLab provides an integration for updating the pipeline statuses on GitHub. This is especially useful if using GitLab for CI/CD only. This project integration is separate from the [instance wide GitHub integration](https://docs.gitlab.com/ee/user/project/import/github.html#mirroring-and-pipeline-status-sharing) and is automatically configured on [GitHub import.](https://docs.gitlab.com/ee/integration/github.html)
