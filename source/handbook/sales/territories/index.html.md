---
layout: handbook-page-toc
title: "Sales Territories"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Process to Request Update 

### Territory Ownership (Sales) 

1. Create an issue in the **Sales Operations** project - utilizing the [Territory Change Request template](https://gitlab.com/gitlab-com/sales-team/field-operations/sales-operations/issues/new?issuable_template=Territory_Change_Request)
1. Follow the directions within the template & provide all the requested details
     - If **Individual Contributor** is requesting the change, ADD your manager to the `/assign` command
     - If **Manager** is requesting change, submit issue & it will auto-assign to Sales Ops
     - **Please note** Operations makes alignment changes **once** at the end of each month, only exception would be a Sales New Hire. 
1. `@tav_scott` to update SFDC
1. Change made on Territory Management document by `@tav_scott` **after** change in system has been made.
1. `@bethpeterson` to update LeanData download updated csv once change confirmed by `@tav_scott`.
1. Territory Management updates will be uploaded to LeanData by `@bethpeterson` **after** change in system has been made. 

### SDR Alignment (Marketing)

1. Create an merge request
1. Update `SDR` column with name of new SDR to cover territory.
     - If **Individual Contributor**, assign the merge request to your direct Manager for approval prior to assigning it to MktgOps
     - If **Manager**, assign the merge request to MktgOps
1. MktgOps processes these reqeusts on a **monthly** basis. 

#### Updating these tables without updating Operations will not be reflected in our various systems causing all reports and routing to be incorrect!   
{:.no_toc}

**Questions?** Ask in `#sales` slack channel pinging `@sales-ops`. 



## Region/Vertical
{:.no_toc}
  * **VP Commercial Sales** ([Mid-Market](#mid-market-segment) & [Small Business](#small-business-segment)): Ryan O'Nell
  * **[APAC](#apac)**: Anthony McMahon, Regional Director
  * **[Europe, Middle East and Africa](#emea)**: TBH, Regional Director
  * **[North America - US East](#us-east)**: Mark Rogge, Regional Director
  * **[North America - US West](#us-west)**: Haydn Mackay, Regional Director
  * **[Public Sector](#public-sector)**: Paul Almeida, Director of Federal Sales

## Territories

### Large

#### AMER

For the United States, the following rules apply to all accounts **except** government agencies or publicly-funded educational institutions or departments, including those at private universities (Johns Hopkins Applied Physics Lab, for example). Government agencies and publicly-funded educational institutions will be managed by our [Public Sector](#public-sector) team.

For other countries outside AMER, governments agencies will be handled by the territory owner.

**Named Account identifiers will be updated in near future and currently are being worked on by the SalesOps team (status as of 2020-01-23)**

##### Area Sales Manager
{:.no_toc}

* **NA East - Named Accounts**: Adam Johnson
* **NA East - South East**: Tom Plumadore
* **NA East - North East**: Sheila Walsh
* **NA West - Rockies/SoCal**: James Roberts
* **NA West - Bay Area**: Alan Cooke
* **NA West - PNW/MidWest**: Timm Ideker

| Sub-Region     | Area           | **Territory Name**                     | Sales              | SDR                |
| :------------- | :------------- | :--------------------------------- | :----------------- | :----------------- |
| LATAM          | Brazil         | **Large-AMER-Brazil**                  | Jim Torres         | Bruno Lazzarin     |
| LATAM          | Chile          | **Large-AMER-Chile**                   | Jim Torres         | Bruno Lazzarin     |
| LATAM          | Mexico         | **Large-AMER-Mexico**                  | Carlos Dominguez   | Bruno Lazzarin     |
| LATAM          | Rest of LATAM  | **Large-AMER-SoCenAmer**               | Jim Torres         | Bruno Lazzarin     |
| NA East        | Southeast      | **Large-AMER-Central Gulf Carolinas**  | Chris Graham       | Shakarra MCGuire   |
| NA East        | Northeast      | **Large-AMER-Eastern Canada**          | Peter McCracken    | Aashish Sharma     |
| NA East        | Northeast      | **Large-AMER-Lake Michigan**           | Tim Kuper          | Marcus Stangl      |
| NA East        | Northeast      | **Large-AMER-MA**                      | Tony Scafidi       | Bill Zaferopolos   |
| NA East        | Northeast      | **Large-AMER-Manhattan**               | Liz Corring        | Max Chadliev       |
| NA East        | Northeast      | **Large-AMER-New England**             | Tony Scafidi       | Bill Zaferopolos   |
| NA East        | Mid-Atlantic   | **Large-AMER-Mid-Atlantic**            | Katherine Evans    | Kelsey Steyn       |
| NA East        | Mid-Atlantic   | **Large-AMER-NY/NJ**                   | Paul Duffy         | Andrew Glidden     |
| NA East        | South Central  | **Large-AMER-North TX/LA**             | Chip Digirolamo    | Suzy Verdin        |
| NA East        | South Central  | **Large-AMER-Ohio Valley**             | Ruben Govender     | Morgen Smith       |
| NA East        | South Central  | **Large-AMER-South TX**                | Matt Petrovick     | Brandon Brooks     |
| NA East        | SunshinePeach  | **Large-AMER-SunshinePeach**           | Jim Bernstein      | Bill Zaferopolos   |
| NA West        | Midwest        | **Large-AMER-Midwest**                 | Timmothy Ideker    | Michael Major      |
| NA West        | NorCal         | **Large-AMER-NorCal**                  | Philip Camillo     | Isaac Mondesir     |
| NA West        | PNW            | **Large-AMER-PNW**                     | Timmothy Ideker    | Michael Major      |
| NA West        | SoCal          | **Large-AMER-SoCal**                   | James Roberts      | Matthew MacFarlane |
| NA West        | Southwest      | **Large-AMER-Southwest**               | Rick Walker        | Blake Chalfant-Kero|
| NA East |  | **Named Accounts** | Mark Bell | Steven Cull |
| NA East |  | **Named Accounts** | John May | Ryan Kimball |
| NA East |  | **Named Accounts** | Jordan Goodwin | Marcus Stangl |
| NA East |  | **Named Accounts** | David Wells | Kaleb Hill |
| NA East |  | **Named Accounts** | Larry Biegel | Geraldine Lee |
| NA East |  | **Named Accounts** | John Orvos | Ryan Kimball |
| NA West |  | **Named Accounts** | Chris Cornacchia | Jesse Muehlbauer |
| NA West |  | **Named Accounts** | Yvonne Zwolinski | Blake Chalfant-Kero |
| NA West |  | **Named Accounts** | John Williams | James Altheide |
| NA West |  | **ENT-MW-Named 1** | Adam Olson | Paul Oakley | 
| NA West |  | **ENT-MW-Named 2** | TBD| Michael Major |
| NA West |  | **ENT-PNW-Named 1** | Joe Drumtra | Eduardo Gonzalez |
| NA West |  | **ENT-PNW-Named 2** | Chris Mayer | Suzy Verdin |
| NA West |  | **Named Accounts** | Robert Hyry | Jesse Muehlbauer |
| NA West |  | **Named Accounts** | Brad Downey | Matthew MacFarlane |
| NA West |  | **ENT-NC-Named SF1** | Mike Walsh | Danae Villarreal |
| NA West |  | **ENT-NC-Named SF2** | Mike Nevolo | James Altheide |
| NA West |  | **ENT-NC-Named Santa Clara 1** | Nico Ochoa | Madison Taft |
| NA West |  | **ENT-NC-Named Santa Clara 2** | Joe Miklos | Danae Villarreal |
| NA West |  | **ENT-NC-Named Santa Clara 3** | Robert van Leer | Madison Taft |
| NA West |  | **ENT-NC-Named Santa Clara 4** | Dina Morgan | Aaron Young |

#### Public Sector

| Sub-Region    | **Territory Name**                             | Strategic Account Leader | Inside Sales Rep  |
| :------------ | :------------------------------------- | :----------------------- | :---------------- |
| Public Sector | **Federal - Civilian-1**               | TBH       | Christine Saah    |
| Public Sector | **Federal - Civilian-2**               | Susannah Reed        | Christine Saah    |
| Public Sector | **Federal - Civilian-3**               | Kyle Goodwin        | Bill Duncan       |
| Public Sector | **Federal - Civilian-4**                   | Russ Wilson          | Bill Duncan       |
| Public Sector | **Federal - Civilian-5**                   | Jim Riley (temp)     | Bill Duncan       |
| Public Sector | **Federal - Civilian-6**                   | Jim Riley (temp)     | Christine Saah    |
| Public Sector | **Federal - Civilian-7**                   | Jim Riley (temp)     | Christine Saah    |
| Public Sector | **Federal - Civilian-8**                   | Jim Riley (temp)     | Bill Duncan       |
| Public Sector | **State and Local (SLED East)**            | Dan Samson           | Alexis Shaw       |
| Public Sector | **State and Local (SLED West)**            | Dave Lewis           | Alexis Shaw       |
| Public Sector | **State and Local (SLED Central)**         | Matt Stamper         | Alexis Shaw       |
| Public Sector | **State and Local (SLED South)**           | Mark Williams        | Alexis Shaw       |
| Public Sector | **Federal - DoD-Air Force-1**              | Ralph Kompare        | Craig Pepper      |
| Public Sector | **Federal - DoD-Air Force-2**              | Ralph Kompare        | Craig Pepper      |
| Public Sector | **Federal - DoD-Unified Commands**         | Ralph Kompare        | Craig Pepper      |
| Public Sector | **Federal - DoD-Navy-1**                   | Melissa Ard          | Craig Pepper      |
| Public Sector | **Federal - DoD-Navy-2**                   | Chris Rennie         | Craig Pepper      |
| Public Sector | **Federal - DoD-Army-1**                   | Allison Mueller      | Margaret Sheridan |
| Public Sector | **Federal - DoD-Army-2**                   | Allison Mueller (temp)| Margaret Sheridan |
| Public Sector | **Federal - DoD-Agencies**                 | Keith Barnes (temp)  | Margaret Sheridan |
| Public Sector | **Federal - Intel-1**                      | Marc Kriz            | Jay Jewell        |
| Public Sector | **Federal - Intel-2**                      | Marc Kriz            | Jay Jewell        |
| Public Sector | **Federal - Channel (System Integrators)** | Mark Robinson        | Jay Jewell        |


#### APAC

| Sub-Region     | Area           | **Territory Name**                     | Sales              | SDR                |
| :------------- | :------------- | :--------------------------------- | :----------------- | :----------------- |
| ANZ            | ANZ            | **Large-APAC-AUS Northern Terriories** | Sarah Orell        | Glenn Perez        |
| ANZ            | ANZ            | **Large-APAC-AUS Queensland**          | Sarah Orell        | Glenn Perez        |
| ANZ            | ANZ            | **Large-APAC-AUS South Australia**     | Danny Petronio     | Glenn Perez        |
| ANZ            | ANZ            | **Large-APAC-AUS Sydney**              | Rob Hueston        | Glenn Perez        |
| ANZ            | ANZ            | **Large-APAC-AUS Victoria**            | Danny Petronio     | Glenn Perez        |
| ANZ            | ANZ            | **Large-APAC-AUS Western Australia**   | Rob Hueston        | Glenn Perez        |
| ANZ            | ANZ            | **Large-APAC-Canberra**                | Sarah Orell        | Glenn Perez        |
| ANZ            | ANZ            | **Large-APAC-North Sydney**            | Rob Hueston        | Glenn Perez        |
| ANZ            | ANZ            | **Large-APAC-NZ**                      | Sarah Orell        | Glenn Perez        |
| Asia Central   | Asia Central   | **Large-APAC-Central Asia**            | Rob Hueston        | Glenn Perez        |
| Asia Central   | Asia Central   | **Large-APAC-Kazakhstan**              | Rob Hueston        | Glenn Perez        |
| Asia SE        | Southeast Asia | **Large-APAC-Cambodia**                | Danny Petronio     | Aletha Alfarania   |
| Asia SE        | Southeast Asia | **Large-APAC-Indonesia**               | Rob Hueston        | Aletha Alfarania   |
| Asia SE        | Southeast Asia | **Large-APAC-Malaysia**                | Rob Hueston        | Aletha Alfarania   |
| Asia SE        | Southeast Asia | **Large-APAC-Myanmar**                 | Danny Petronio     | Aletha Alfarania   |
| Asia SE        | Southeast Asia | **Large-APAC-Philippines**             | Rob Hueston        | Aletha Alfarania   |
| Asia SE        | Southeast Asia | **Large-APAC-Thailand**                | Danny Petronio     | Aletha Alfarania   |
| Asia SE        | Southeast Asia | **Large-APAC-Viet Nam**                | Danny Petronio     | Aletha Alfarania   |
| Asia SE        | Singapore      | **Large-APAC-Singapore**               | Danny Petronio     | Glenn Perez        |
| Asia South     | India          | **Large-APAC-India**                   | Danny Petronio     | Minsu Han          |
| China          | China          | **Large-APAC-China**                   | Danny Petronio     | Aletha Alfarania   |
| China          | Taiwan         | **Large-APAC-Taiwan**                  | Danny Petronio     | Aletha Alfarania   |
| Japan          | Japan          | **Large-APAC-Japan**                   | Rob Hueston        | Glenn Perez        |
| Korea          | Korea          | **Large-APAC-Korea**                   | Woosang Lee        | Minsu Han          |


#### EMEA

| Sub-Region     | Area           | **Territory Name**                     | Sales              | SDR                |
| :------------- | :------------- | :--------------------------------- | :----------------- | :----------------- |
| Europe Central | Europe Central | **Large-EMEA-BeNeLux**                 | Nasser Mohunlol    | Shay Fleming  |
| Europe Central | Europe Central | **Large-EMEA-CH/AUS**                  | Thomas Bosshard    | Peter Kunkli       |
| Europe Central | Germany        | **Large-EMEA-Germany**                 | Rene Hoferichter   | Gábor Zaparkanszky |
| Europe East    | Europe East    | **Large-EMEA-Eastern Europe**          | Vadim Rusin        | Arianna Bellino    |
| Europe South   | Europe South   | **Large-EMEA-France**                  | Aleksandar Bosnic  | Magali Bressan     |
| Europe South   | Europe South   | **Large-EMEA-Italy**                   | Vadim Rusin        | Arianna Bellino    |
| Europe South   | Europe South   | **Large-EMEA-Portugal**                | Vadim Rusin        | Camilo Villanueva    |
| Europe South   | Europe South   | **Large-EMEA-Spain**                   | Vadim Rusin        | Camilo Villanueva    |
| UKI   | UKI   | **Large-EMEA-UKI**                     | Robbie Byrne       | UKI Large^          |
| MEA            | MEA            | **Large-EMEA-MEA**                     | MEA Large Sales^^    | Camilo Villanueva    |
| Nordics        | Nordics        | **Large-EMEA-Nordics**                 | Annette Kristensen | Camilo Villanueva  |
|  |  | **Named Accounts** | Justin Haley | Chris Loudon |
|  |  | **Named Accounts** | Hugh Christey | Magali Bressan |
|  |  | **Named Accounts** | Timo Schuit | Peter Kunkli |
|  |  | **Named Accounts** | Phillip Smith | Camilo Villanueva |

^ `SDR` = `UKI Large`: Round robin group consisting of Chris Loudon, Daniel Phelan, Shay Fleming      
^^ `Sales` = `MEA Large Sales`: Round robin group consisting of Vadim Rusin, Phillip Smith      

### Mid-Market

#### AMER

| Sub-Region     | Area           | **Territory Name**                  | Sales                         | SDR              |
| :------------- | :------------- | :------------------------------ | :---------------------------- | :--------------- |
| LATAM          | Brazil         | **MM-AMER-Brazil**                  | Romer Gonzalez                | Bruno Lazzarin   |
| LATAM          | Rest of LATAM  | **MM-AMER-SoCenAmer**               | Romer Gonzalez                | Bruno Lazzarin   |
| NA East        | US East        | **MM-AMER-BosRI**                   | Todd Lauver                   | Lanice Sims      |
| NA East        | US East        | **MM-AMER-Chicago**                 | Jenny Kline                   | Phillip Knorr    |
| NA East        | US East        | **MM-AMER-East**                    | Jeff Lackey                   | Evan Mathis      |
| NA East        | US East        | **MM-AMER-Greater Quebec**          | Alyssa Belardi                | Lanice Sims      |
| NA East        | US East        | **MM-AMER-IL**                      | Jenny Kline                   | Kelsey Claflin    |
| NA East        | US East        | **MM-AMER-Manhattan South**         | Alyssa Belardi                | Lanice Sims      |
| NA East        | US East        | **MM-AMER-Mass**                    | Todd Lauver                   | Lanice Sims      |
| NA East        | US East        | **MM-AMER-Mn**                      | Kyla Gradin                   | Phillip Knorr    |
| NA East        | US East        | **MM-AMER-New England**             | Sharif Bennett                | Kelsey Claflin     |
| NA East        | US East        | **MM-AMER-NJ/DC**                   | Jeff Lackey                   | Evan Mathis      |
| NA East        | US East        | **MM-AMER-NYPhili**                 | Steve Xu                      | Phillip Knorr    |
| NA East        | US East        | **MM-AMER-Ontario**                 | Kyla Gradin                   | Phillip Knorr    |
| NA East        | US East        | **MM-AMER-PeterTampa**              | Daniel Parry                  | Evan Mathis      |
| NA East        | US East        | **MM-AMER-SE West**                 | Daniel Parry                  | Evan Mathis      |
| NA East        | US East        | **MM-AMER-SunshinePeach**           | Jeff Lackey                   | Evan Mathis      |
| NA East        | US East        | **MM-AMER-WiPa**                    | Steve Xu                      | Phillip Knorr    |
| NA West        | US West        | **MM-AMER-AZ/NV/HIGreater LA**      | Douglas Robbin                | Alexander Yoseph |
| NA West        | US West        | **MM-AMER-DowntownSF**              | Leigh Motheral                | Ariah Curtis     |
| NA West        | US West        | **MM-AMER-East/Dallas**             | Laura Shand                  | Josh Weatherford |
| NA West        | US West        | **MM-AMER-Greater Austin**          | Scott Larson                  | Josh Weatherford |
| NA West        | US West        | **MM-AMER-Jefferson**               | Christopher Chiappe           | Da'Neil Olsen    |
| NA West        | US West        | **MM-AMER-LA Core**                 | Douglas Robbin                | Alexander Yoseph |
| NA West        | US West        | **MM-AMER-Mt**                      | Matt Doerfler                 | Josh Weatherford |
| NA West        | US West        | **MM-AMER-Mt-IdOr**                 | Laura Shand                 | Josh Weatherford |
| NA West        | US West        | **MM-AMER-Mt-Utah**                 | Matt Doerfler                 | Josh Weatherford |
| NA West        | US West        | **MM-AMER-Mt-Wext/TX**              | Scott Larson                 | Josh Weatherford |
| NA West        | US West        | **MM-AMER-Palo Alto**               | James Komara                  | Da'Neil Olsen    |
| NA West        | US West        | **MM-AMER-Peninsula**               | Christopher Chiappe           | Da'Neil Olsen    |
| NA West        | US West        | **MM-AMER-SF**                      | Jennifer MacVicar             | Ariah Curtis     |
| NA West        | US West        | **MM-AMER-SoCal/Irvine**            | Rashad Bartholomew            | Alexander Yoseph |
| NA West        | US West        | **MM-AMER-Thousand Oaks**           | James Komara                  | Da'Neil Olsen    |
| NA West        | US West        | **MM-AMER-WaAk**                    | Leigh Motheral                | Ariah Curtis     |


#### APAC

| Sub-Region     | Area           | **Territory Name**                  | Sales                         | SDR              |
| :------------- | :------------- | :------------------------------ | :---------------------------- | :--------------- |
| ANZ            | ANZ            | **MM-APAC-ANZ**                     | Julie Manalo                  | Glenn Perez      |
| Asia Central   | Asia Central   | **MM-APAC-Central Asia**            | Julie Manalo                  | Glenn Perez      |
| China          | China          | **MM-APAC-China**                   | Julie Manalo                  | Aletha Alfarania |
| Japan          | Japan          | **MM-APAC-Japan**                   | Julie Manalo                  | Glenn Perez      |
| Asia SE        | Southeast Asia | **MM-APAC-SE Asia**                 | Julie Manalo                  | Aletha Alfarania |
| Asia South     | Asia South     | **MM-APAC-South Asia**              | Ishan Padgotra / Julie Manalo | Minsu Han        |
| Korea          | Korea          | **MM-APAC-South Korea**             | Julie Manalo                  | Minsu Han        |


#### EMEA

| Sub-Region     | Area           | **Territory Name**                  | Sales                         | SDR              |
| :------------- | :------------- | :------------------------------ | :---------------------------- | :--------------- |
| Europe Central | Europe Central | **MM-EMEA-Amsterdam**               | Hans Frederiks                | Kristof Eger     |
| Europe Central | Europe Central | **MM-EMEA-Austria**                 | Conor Brady                   | Kristof Eger     |
| Europe Central | Europe Central | **MM-EMEA-Belgium**                 | Hans Frederiks                | Kristof Eger     |
| Europe Central | Europe Central | **MM-EMEA-Rest of Netherlands**     | Hans Frederiks                | Kristof Eger     |
| Europe Central | Europe Central | **MM-EMEA-Switzerland**             | Conor Brady                   | Kristof Eger     |
| Europe Central | Germany        | **MM-EMEA-Berlin**                  | Chris Willis                  | Rahim Abdullayev |
| Europe Central | Germany        | **MM-EMEA-Frankfurt**               | Chris Willis                  | Rahim Abdullayev |
| Europe Central | Germany        | **MM-EMEA-Hamburg**                 | Conor Brady                   | Kristof Eger     |
| Europe Central | Germany        | **MM-EMEA-Munich**                  | Anthony Ogunbowale-Thomas     | Rahim Abdullayev |
| Europe South   | Europe South   | **MM-EMEA-Southern Europe**         | Anthony Ogunbowale-Thomas     | Rahim Abdullayev |
| Europe South   | Europe South   | **MM-EMEA-Paris**                   | Israa Mahros                  | Camille Dios     |
| Europe South   | Europe South   | **MM-EMEA-Rest of France**          | Israa Mahros                  | Camille Dios     |
| Europe East    | Eastern Europe | **MM-EMEA-Eastern Europe**          | Israa Mahros                  | Camille Dios     |
| Europe East    | Russia         | **MM-EMEA-Russia**                  | Chris Willis                  | Rahim Abdullayev |
| MEA            | MEA            | **MM-EMEA-Israel**                  | Anthony Ogunbowale-Thomas     | Rahim Abdullayev |
| MEA            | MEA            | **MM-EMEA-ME**                      | Israa Mahros                  | Camille Dios     |
| MEA            | MEA            | **MM-EMEA-MEA**                     | Israa Mahros                  | Camille Dios     |
| Nordics        | Nordics        | **MM-EMEA-Finland**                 | Hans Frederiks                | Kristof Eger     |
| Nordics        | Nordics        | **MM-EMEA-Nordics 1**               | Hans Frederiks                | Kristof Eger     |
| Nordics        | Nordics        | **MM-EMEA-Nordics 2**               | Hans Frederiks                | Kristof Eger     |
| UKI            | UKI            | **MM-EMEA-Camden/Islington-Metro**  | Anthony Ogunbowale-Thomas     | Rahim Abdullayev |
| UKI            | UKI            | **MM-EMEA-Canary Wharf-Metro**      | Chris Willis                  | Rahim Abdullayev |
| UKI            | UKI            | **MM-EMEA-East London**             | Anthony Ogunbowale-Thomas     | Rahim Abdullayev |
| UKI            | UKI            | **MM-EMEA-Ireland**                 | Conor Brady                   | Kristof Eger     |
| UKI            | UKI            | **MM-EMEA-Lambeth/Southwark**       | Anthony Ogunbowale-Thomas     | Rahim Abdullayev |
| UKI            | UKI            | **MM-EMEA-Lambeth/Southwark-Metro** | Chris Willis                  | Rahim Abdullayev |
| UKI            | UKI            | **MM-EMEA-Rest of UK**              | Conor Brady                   | Kristof Eger     |
| UKI            | UKI            | **MM-EMEA-The City**                | Chris Willis                  | Rahim Abdullayev |
| UKI            | UKI            | **MM-EMEA-Westminister-Metro**      | Chris Willis                  | Rahim Abdullayev |


### SMB

#### AMER

| Sub-Region     | Area           | **Territory Name**           | Sales                            | SDR                   |
| :------------- | :------------- | :----------------------- | :------------------------------- | :-------------------- |
| LATAM          | Brazil         | **SMB-AMER-Brazil**          | Romer Gonzalez                   | Bruno Lazzarin |
| LATAM          | Rest of LATAM  | **SMB-AMER-SoCenAmer**       | Romer Gonzalez                   | Bruno Lazzarin |
| NA East        | US East        | **SMB-AMER-Florida**         | Anthony Feldman                  | AMER Commercial - SMB^ |
| NA East        | US East        | **SMB-AMER-Great Lakes**     | Anthony Feldman                  | AMER Commercial - SMB^ |
| NA East        | US East        | **SMB-AMER-Illinois**        | Matthew Walsh                    | AMER Commercial - SMB^ |
| NA East        | US East        | **SMB-AMER-Northeast**       | Michael Miranda                  | AMER Commercial - SMB^ |
| NA East        | US East        | **SMB-AMER-Southeast**       | Kaley Johnson                    | AMER Commercial - SMB^ |
| NA West        | US West        | **SMB-AMER-Mountain**        | Adam Pestreich / Michael Miranda | AMER Commercial - SMB^ |
| NA West        | US West        | **SMB-AMER-Northwest**       | Marsja Jones                     | AMER Commercial - SMB^ |
| NA West        | US West        | **SMB-AMER-Southwest**       | Adam Pestreich                   | AMER Commercial - SMB^ |
| NA West        | US West        | **SMB-AMER-Texas**           | Brooke Williamson                | AMER Commercial - SMB^ |
| NA West        | US West        | **SMB-AMER-Washington**      | Brooke Williamson                | AMER Commercial - SMB^ |
| TBD            | TBD            | **SMB-AMER-TBD**             | Carrie Nicholson                 | AMER Commercial - SMB^ |

^ `SDR` = `AMER Commercial - SMB`: Round robin group consisting of Jenny Chapman and Jada Rogers


#### APAC

| Sub-Region     | Area           | **Territory Name**           | Sales                            | SDR                   |
| :------------- | :------------- | :----------------------- | :------------------------------- | :-------------------- |
| ANZ            | ANZ            | **SMB-APAC-ANZ**             | Wayne Zhao                       | Glenn Perez           |
| Asia Central   | Asia Central   | **SMB-APAC-Central Asia**    | Wayne Zhao                       | Glenn Perez           |
| China          | China          | **SMB-APAC-China**           | Wayne Zhao                       | Aletha Alfarania      |
| Japan          | Japan          | **SMB-APAC-Japan**           | Wayne Zhao                       | Glenn Perez           |
| Korea          | Korea          | **SMB-APAC-Korea**           | Wayne Zhao                       | Minsu Han             |
| Asia SE        | Southeast Asia | **SMB-APAC-SE Asia**         | Ishan Padgotra                   | Aletha Alfarania      |
| Asia South     | Asia South     | **SMB-APAC-South Asia**      | Ishan Padgotra                   | Minsu Han             |


#### EMEA

| Sub-Region     | Area           | **Territory Name**           | Sales                            | SDR                   |
| :------------- | :------------- | :----------------------- | :------------------------------- | :-------------------- |
| Europe Central | Europe Central | **SMB-EMEA-BeNeLux**         | Lisa vdKooij                     | EMEA Commercial - SMB^ |
| Europe Central | DACH           | **SMB-EMEA-North Germany**   | Bastian van der Stel             | EMEA Commercial - SMB^ |
| Europe Central | DACH           | **SMB-EMEA-South Germany**   | Vilius Kavaliauskas              | EMEA Commercial - SMB^ |
| Europe Central | DACH           | **SMB-EMEA-Rest of DACH**    | Vilius Kavaliauskas              | EMEA Commercial - SMB^ |
| Europe East    | Eastern Europe | **SMB-EMEA-Eastern Europe**  | Lisa vdKooij                     | EMEA Commercial - SMB^ |
| Europe South   | Europe South   | **SMB-EMEA-France**          | Ross Mawhinney                   | EMEA Commercial - SMB^ |
| Europe South   | Europe South   | **SMB-EMEA-Southern Europe** | Ross Mawhinney                   | EMEA Commercial - SMB^ |
| MEA            | MEA            | **SMB-EMEA-MEA**             | Vilius Kavaliauskas              | EMEA Commercial - SMB^ |
| Nordics        | Nordics        | **SMB-EMEA-Nordics**         | Daisy Miclat                     | EMEA Commercial - SMB^ |
| UKI            | UKI            | **SMB-EMEA-UKI**             | Daisy Miclat                     | EMEA Commercial - SMB^ |

^ `SDR` = `EMEA Commercial - SMB`: Round robin group consisting of Wiam Aissaoui, Alexander Demblin, Dorde Sumenkovic and Johan Rosendahl


