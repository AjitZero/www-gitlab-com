---
layout: job_family_page
title: "Product Marketing Management"
---

## Manager, Product Marketing
This position is remote based and will report to the Director of Product Marketing.  You will be responsible for building and managing a virtual, world-class global product marketing team.

### Responsibilities
* Recruit, coach and manage product marketing managers that live our values
* Set organization direction, staff for scale and develop the skills and career paths of all team members
* Ensure all our product marketing managers are very effective.
* Promote  and identify underperformance proactively.
* Own the quality, effectiveness and performance of the product marketing team results.
* Measure and improve the happiness and productivity of the team.
* Partner and work closely with Technical Product Marketing, Product Management, Sales, Engineering, Customer Success, and other Marketing functions.
* Make sure the success metrics such as quarterly OKRs are identified, met, and communicated across key stakeholders.

### Requirements
* 8-10 years of product marketing experience.
* Experience with DevOps and/or developer tooling
* Team player with strong intra-personal skills, skilled at project management and cross-functional collaboration.
* 5+ years experience in hiring, scaling, managing and leading a team.
* Understand the difference between managing and leading a team.
* Experience managing external vendors and agencies.
* Enterprise software company experience.
* Passionate about open source and developer tools.
* Strong communication skills.
* You share our [values](/handbook/values), and work in accordance with those values.
* [Leadership at GitLab](https://about.gitlab.com/handbook/leadership/#management-group)
* Ability to use GitLab

## Hiring Process
Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his job title on our [team page](/company/team).

* Qualified candidates will be invited to schedule a [screening call](/handbook/hiring/interviewing/#screening-call) with one of our Global Recruiters.
** Qualified candidates will be invited to schedule a [screening call](/handbook/hiring/interviewing/) with one of our Global Recruiters.
* Candidates will then be invited to a 45 minute interview with our Director of Product Marketing.
* Candidates will then be invited to a 45 minute interview with our Manager of Product Marketing team.
* Candidates will then be invited to a 45 minute interview with our Manager of Technical Marketing team.
* Candidates will then be invited to schedule a 45 minute interview with our VP, Product Management.
* Next, candidates may be invited to schedule a 45 minute interview with the Chief Marketing Officer.
* Finally, our CEO may choose to conduct a final interview.

Additional details about our process can be found on our [hiring page](/handbook/hiring).

## Manager, Technical Marketing
This position is remote based and will report to the Director of Product Marketing.  You will be responsible for building and managing a virtual, world-class global technical marketing team.

### Responsibilities
* Recruit, coach and manage technical product marketing managers that live our [values](/handbook/values)
* Set organization direction, staff for scale and develop the skills and career paths of all team members
* Ensure all our technical product marketing managers are very effective.
* Promote  and identify underperformance proactively.
* Own the quality, effectiveness and performance of the product marketing team results.
* Measure and improve the happiness and productivity of the team.
* Partner and work closely with Product Marketing, Product Management, Sales, Engineering, Customer Success, and other Marketing functions.
* Make sure the success metrics such as quarterly OKRs are identified, met, and communicated across key stakeholders.

### Requirements
* 5+ years of technical product marketing experience.
* Experience with DevOps and/or developer tooling
* Team player with strong intra-personal skills, skilled at project management and cross-functional collaboration.
* 5+ years experience in hiring, scaling, managing and leading a team.
* Understand the difference between managing and leading a team.
* Experience managing external vendors and agencies.
* Enterprise software company experience.
* Passionate about open source and developer tools.
* Strong communication skills.
* You share our [values](/handbook/values), and work in accordance with those values.

## Hiring Process
Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his job title on our [team page](/company/team).

* Qualified candidates will be invited to schedule a [screening call](/handbook/hiring/interviewing/#screening-call) with one of our Global Recruiters.
** Qualified candidates will be invited to schedule a [screening call](/handbook/hiring/interviewing/) with one of our Global Recruiters.
* Candidates will then be invited to a 45 minute interview with our Director of Product Marketing.
* Candidates will then be invited to a 45 minute interview with our Manager of Product Marketing team.
* Candidates will then be invited to a 45 minute interview with our Manager of Technical Marketing team.
* Candidates will then be invited to schedule a 45 minute interview with our VP, Product Management.
* Next, candidates may be invited to schedule a 45 minute interview with the Chief Marketing Officer.
* Finally, our CEO may choose to conduct a final interview.

Additional details about our process can be found on our [hiring page](/handbook/hiring).

## Manager, Partner and Channel Marketing
This position is remote based and will report to the Director of Product Marketing.  You will be responsible for building and managing a virtual, world-class global product marketing team focused on partner and channel marketing.

### Responsibilities
* Recruit, coach and manage product marketing managers that live our values.
* Set organization direction, staff for scale and develop the skills and career paths of all team members.
* Ensure all our product marketing managers are very effective.
* Promote  and identify underperformance proactively.
* Own the quality, effectiveness and performance of the product marketing team results.
* Measure and improve the happiness and productivity of the team.
* Partner and work closely with Technical Product Marketing, Product Management, Sales, Engineering, Customer Success, and other Marketing functions.
* Make sure the success metrics such as quarterly OKRs are identified, met, and communicated across key stakeholders.

### Requirements
* 8-10 years of product marketing experience.
* 1-2 years of partner, channel, reseller or alliance marketing.
* Experience with DevOps and/or developer tooling.
* Team player with strong intra-personal skills, skilled at project management and cross-functional collaboration.
* 5+ years experience in hiring, scaling, managing and leading a team.
* Understand the difference between managing and leading a team.
* Experience managing external vendors and agencies.
* Enterprise software company experience.
* Passionate about open source and developer tools.
* Strong communication skills.
* You share our [values](/handbook/values), and work in accordance with those values.
* [Leadership at GitLab](https://about.gitlab.com/handbook/leadership/#management-group)

## Hiring Process
Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his job title on our [team page](/company/team).
* Qualified candidates will be invited to schedule a [screening call](/handbook/hiring/interviewing/) with one of our Global Recruiters.
* Candidates will then be invited to a 45 minute interview with our Director of Product Marketing.
* Candidates will then be invited to a 45 minute interview with our Manager of Product Marketing team.
* Candidates will then be invited to a 45 minute interview with our Manager of Technical Marketing team.
* Candidates will then be invited to schedule a 45 minute interview with our VP, Alliances.
* Next, candidates may be invited to schedule a 45 minute interview with the Chief Marketing Officer.
* Finally, our CEO may choose to conduct a final interview.
* Successful candidates will subsequently be made an offer via email.

Additional details about our process can be found on our [hiring page](/handbook/hiring).

## Manager, Competitive Intelligence
This position is remote based and will report to the Director of Product Marketing.  You will be responsible for building and managing a virtual, world-class global comptetitive intelligence team.

### Responsibilities
* Recruit, coach and manage a competitive intelligence team that lives our values.
* Set organization direction, staff for scale and develop the skills and career paths of all team members.
* Ensure all our competitive intelligence managers are effective and deliver results.
* Own the quality, effectiveness and performance of the competitive intelligence team results.
* Measure and improve the happiness and productivity of the team.
* Promote  and identify underperformance proactively.
* Partner and work closely with the Product Marketing, Technical Marketing, Product Management, Sales, Engineering, Customer Success, and other Marketing functions.
* Make sure the success metrics such as quarterly OKRs are identified, met, and communicated across key stakeholders.

### Requirements
* 8-10 years of product marketing, technical marketing or competitive intelligence experience.
* Experience with DevOps and/or developer tooling.
* Team player with strong intra-personal skills, skilled at project management and cross-functional collaboration.
* 5+ years experience in hiring, scaling, managing and leading a team.
* Understand the difference between managing and leading a team.
* Experience managing external vendors and agencies.
* Enterprise software company experience.
* Passionate about open source and developer tools.
* Strong communication skills.
* You share our [values](/handbook/values), and work in accordance with those values.
* [Leadership at GitLab](https://about.gitlab.com/handbook/leadership/#management-group)

## Hiring Process
Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his job title on our [team page](/company/team).

* Qualified candidates will be invited to schedule a [screening call](/handbook/hiring/interviewing/#screening-call) with one of our Global Recruiters.
** Qualified candidates will be invited to schedule a [screening call](/handbook/hiring/interviewing/) with one of our Global Recruiters.
* Candidates will then be invited to a 45 minute interview with our Director of Product Marketing.
* Candidates will then be invited to a 45 minute interview with our Manager of Product Marketing team.
* Candidates will then be invited to a 45 minute interview with our Manager of Technical Marketing team.
* Candidates will then be invited to schedule a 45 minute interview with our Director, Product Management.
* Next, candidates may be invited to schedule a 45 minute interview with our Chief Marketing Officer.
* Finally, our CEO may choose to conduct a final interview.

Additional details about our process can be found on our [hiring page](/handbook/hiring).
