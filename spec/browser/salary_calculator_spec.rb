require 'spec_helper'

describe 'the salary calculator', :js do
  context 'when the calculator is on its own page' do
    before do
      visit '/handbook/people-group/global-compensation/calculator/'
      select_calculator_field('role', 'Backend Engineer')
    end

    context 'when the position is in a country with a currency on our currencies list' do
      before do
        select_calculator_field('country', 'Netherlands')
        select_calculator_field('area', 'Everywhere else')
      end

      let!(:original_salary) { find('.compensation .compensation-box-amount').text }
      let!(:original_salary_eur) { find('.compensation .converted-local-currency').text }

      let!(:original_base_pay) { find('.compensation .base-compensation-box-amount').text }
      let!(:original_base_pay_eur) { find('.compensation .converted-local-currency-base-range').text }

      it 'shows the salary in USD and in the local currency' do
        expect(find('.compensation .compensation-box-amount')).to have_text(/\$\d+,\d+ - \$\d+,\d+/)
        expect(find('.compensation .converted-local-currency')).to have_text(/\d+,\d+ EUR - \d+,\d+ EUR/)
      end

      it 'does not show a link to copy' do
        expect(page).not_to have_css('#compensation-link')
      end

      it 'changes salary when level changes' do
        select_calculator_field('level', 'Senior')

        expect(find('.compensation .compensation-box-amount')).not_to have_text(original_salary)
        expect(find('.compensation .converted-local-currency')).not_to have_text(original_salary_eur)
      end

      it 'changes variable pay when TTC level changes to one with variable pay' do
        select_calculator_field('level', 'Director')

        expect(page).not_to have_text('There is no variable pay component for this role')
        expect(page).to have_selector('.compensation .variable-compensation-box-amount')
        expect(page).to have_selector('.compensation .converted-local-currency-variable-pay-range')
      end

      it 'changes base pay when TTC level changes to one with variable pay' do
        select_calculator_field('level', 'Director')

        expect(find('.compensation .base-compensation-box-amount')).not_to have_text(original_base_pay)
        expect(find('.compensation .converted-local-currency-base-range')).not_to have_text(original_base_pay_eur)
      end

      it 'difference between base and TTC pay when there is a variable pay' do
        select_calculator_field('level', 'Director')

        base_pay = find('.compensation .base-compensation-box-amount').text
        expect(find('.compensation .compensation-box-amount')).not_to have_text(base_pay)
      end

      it 'changes salary when experience factor changes' do
        select_calculator_field('comparatio', 'Learning the role')

        expect(find('.compensation .compensation-box-amount')).not_to have_text(original_salary)
        expect(find('.compensation .converted-local-currency')).not_to have_text(original_salary_eur)
      end

      it 'changes salary when area changes' do
        select_calculator_field('area', 'Amsterdam')

        expect(find('.compensation .compensation-box-amount')).not_to have_text(original_salary)
        expect(find('.compensation .converted-local-currency')).not_to have_text(original_salary_eur)
      end

      it 'resets area when country changes' do
        select_calculator_field('country', 'Italy')

        expect(page).not_to have_css('.area .title')
        expect(find('.compensation .compensation-box-amount')).not_to have_text(original_salary)
        expect(page).not_to have_css('.compensation .converted-local-currency')
      end
    end

    context 'when the position is in a country without a currency on our currencies list' do
      before do
        select_calculator_field('country', 'Namibia')
        select_calculator_field('area', 'All')
      end

      it 'shows the salary in USD only' do
        expect(find('.compensation .compensation-box-amount')).to have_text(/\$\d+,\d+ - \$\d+,\d+/)
        expect(page).not_to have_css('.compensation .converted-local-currency')
      end
    end

    context 'when the location is on the do-not-hire list' do
      before do
        select_calculator_field('country', 'Spain')
      end

      it 'shows a message' do
        expect(page).to have_css('.js-country-no-hire')
      end
    end
  end

  context 'when the calculator is on an invidual job page' do
    before do
      visit '/job-families/engineering/backend-engineer/'
    end

    context 'when the fields are all filled in' do
      before do
        select_calculator_field('country', 'Netherlands')
        select_calculator_field('area', 'Everywhere else')
      end

      it 'shows a link that shows the same calculator state' do
        original_amount = find('.compensation .compensation-box-amount').text

        visit find('#compensation-link').value

        expect(find('.compensation .compensation-box-amount')).to have_text(original_amount)
      end
    end
  end

  def select_calculator_field(field, value)
    find(".salary-container .#{field} .dropdown-toggle").click

    if %w[country area role].include?(field)
      find(".salary-container .#{field} [type='search']").fill_in(with: value)
    end

    find(".salary-container .#{field} .key", text: value).click
    expect(page).to have_content(value)
  end
end
